package edu.csusb.iset.gmailsettings.core;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class ApplicationLogger {

    private static final long LOG_SIZE_LIMIT = 50000;
    private static final int LOG_FILE_COUNT = 1;
    private static final boolean LOG_FILE_APPEND = true;
    private static final Path LOG_PATH = Constants.APPLICATION_PATH.resolve("Logs");
    @SuppressWarnings("SpellCheckingInspection")
    private static final String LOG_FILE_NAME = "gmailsettings.log";

    /**
     * @param name A name for the logger.  See java.util.logging.Logger.
     * @return application's Logger
     */
    public static Logger getLogger(String name) {
        Logger logger = Logger.getLogger(name);
        try {
            logger.addHandler(getFileHandler());
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Could not add FileHandler", e);
        }

        return logger;
    }

    /**
     * @return application's FileHandler
     * @throws IOException if there are IO problems creating directories or FileHandler
     */
    public static FileHandler getFileHandler() throws IOException {
        if (!Files.exists(LOG_PATH)) {
            Files.createDirectories(LOG_PATH);
        }

        FileHandler fileHandler = new FileHandler(getPath(LOG_FILE_NAME).toString(), LOG_SIZE_LIMIT, LOG_FILE_COUNT, LOG_FILE_APPEND);
        fileHandler.setFormatter(new SimpleFormatter());
        return fileHandler;
    }

    /**
     * @param fileName A name for the log file.
     * @return application log's Path
     */
    public static Path getPath(String fileName) {
        return LOG_PATH.resolve(fileName);
    }
}
