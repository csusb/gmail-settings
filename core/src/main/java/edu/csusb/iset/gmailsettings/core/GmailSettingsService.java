/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.csusb.iset.gmailsettings.core;

import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.*;
import com.google.auth.http.HttpCredentialsAdapter;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import org.dom4j.DocumentException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

/**
 * @author Chad Cordero <ccordero@csusb.edu>
 */
public class GmailSettingsService {

    /**
     * Scopes
     */
    private static final List<String> SCOPES = Arrays.asList(GmailScopes.GMAIL_SETTINGS_BASIC, GmailScopes.GMAIL_SETTINGS_SHARING);

    private static Gmail service;

    /**
     * Constructor for GmailSettingsService
     *
     * @param userName the username of account
     * @throws IOException                 if client service fails
     * @throws ConnectionNotFoundException if no connection is found
     * @throws DocumentException           if error occurs parsing connections file
     */
    public GmailSettingsService(String userName) throws IOException, ConnectionNotFoundException, DocumentException {
        Connections connections = new Connections();
        String domain = userName.substring(userName.indexOf("@") + 1);
        Path serviceAccountCredentials = connections.getCredentials(domain);

        service = getGmailService(userName, serviceAccountCredentials);
    }

    /**
     * Build and return an authorized Gmail client service.
     *
     * @return an authorized Gmail client service
     * @throws IOException if credentials could not be created from the stream
     */
    private static Gmail getGmailService(String userEmail, Path serviceAccountCredentials) throws IOException {
        HttpTransport httpTransport = new NetHttpTransport();
        JacksonFactory jsonFactory = new JacksonFactory();
        GoogleCredentials credentials = ServiceAccountCredentials.fromStream(Files.newInputStream(serviceAccountCredentials))
                .createScoped(SCOPES)
                .createDelegated(userEmail);
        HttpRequestInitializer requestInitializer = new HttpCredentialsAdapter(credentials);

        return new Gmail.Builder(httpTransport, jsonFactory, requestInitializer)
                .setApplicationName(Constants.APPLICATION_NAME)
                .build();
    }

    /**
     * Gets auto-forwarding settings
     *
     * @return user's AutoForwarding or null
     * @throws IOException if AutoForwarding could not be retrieved
     */
    public AutoForwarding getAutoForwarding() throws IOException {
        return service.users().settings().getAutoForwarding("me").execute();
    }

    /**
     * Gets IMAP settings
     *
     * @return user's ImapSettings or null
     * @throws IOException if ImapSettings could not be retrieved
     */
    public ImapSettings getImapSettings() throws IOException {
        return service.users().settings().getImap("me").execute();
    }

    /**
     * Gets POP settings
     *
     * @return user's PopSettings or null
     * @throws IOException if PopSettings could not be retrieved
     */
    public PopSettings getPopSettings() throws IOException {
        return service.users().settings().getPop("me").execute();
    }

    /**
     * Gets vacation settings
     *
     * @return user's VacationSettings or null
     * @throws IOException if VacationSettings could not be retrieved
     */
    public VacationSettings getVacationSettings() throws IOException {
        return service.users().settings().getVacation("me").execute();
    }

    /**
     * Updates the auto-forwarding settings
     *
     * @param settings the AutoForwarding settings
     * @return updated AutoForwarding or null
     * @throws IOException if AutoForwarding could not be retrieved
     */
    public AutoForwarding updateAutoForwarding(AutoForwarding settings) throws IOException {
        return service.users().settings().updateAutoForwarding("me", settings).execute();
    }

    /**
     * Updates IMAP settings
     *
     * @param settings the ImapSettings
     * @return updated ImapSettings or null
     * @throws IOException if ImapSettings could not be updated
     */
    public ImapSettings updateImapSettings(ImapSettings settings) throws IOException {
        return service.users().settings().updateImap("me", settings).execute();
    }

    /**
     * Updates POP settings
     *
     * @param settings the PopSettings
     * @return updated PopSettings or null
     * @throws IOException if PopSettings could not be updated
     */
    public PopSettings updatePopSettings(PopSettings settings) throws IOException {
        return service.users().settings().updatePop("me", settings).execute();
    }

    /**
     * Updates vacation responder settings
     *
     * @param settings the VacationSettings
     * @return updated VacationSettings or null
     * @throws IOException if VacationSettings could not be updated
     */
    public VacationSettings updateVacationSettings(VacationSettings settings) throws IOException {
        return service.users().settings().updateVacation("me", settings).execute();
    }

    /**
     * Creates a forwarding address
     *
     * @param settings the ForwardingAddress
     * @return created ForwardingAddress
     * @throws IOException if ForwardingAddress could not be created
     */
    public ForwardingAddress createForwardingAddress(ForwardingAddress settings) throws IOException {
        return service.users().settings().forwardingAddresses().create("me", settings).execute();
    }

    /**
     * Deletes a forwarding address
     *
     * @param forwardingEmail the forwarding email to be deleted
     * @throws IOException if forwarding email could not be deleted
     */
    public void deleteForwardingAddress(String forwardingEmail) throws IOException {
        service.users().settings().forwardingAddresses().delete("me", forwardingEmail).execute();
    }

    /**
     * Gets list of forwarding address settings
     *
     * @return list of ForwardingAddress or null
     * @throws IOException if ListForwardingAddressesResponse could not be retrieved
     */
    public List<ForwardingAddress> listForwardingAddresses() throws IOException {
        ListForwardingAddressesResponse listForwardingAddressesResponse = service.users().settings().forwardingAddresses().list("me").execute();

        return listForwardingAddressesResponse != null ? listForwardingAddressesResponse.getForwardingAddresses() : null;
    }

    /**
     * Creates a send-as address
     *
     * @param settings the SendAs
     * @return created SendAs
     * @throws IOException if SendAs could not be created
     */
    public SendAs createSendAs(SendAs settings) throws IOException {
        return service.users().settings().sendAs().create("me", settings).execute();
    }

    /**
     * Deletes a send-as address
     *
     * @param sendAsEmail The send-as alias to be deleted
     * @throws IOException if send-as email could not be deleted
     */
    public void deleteSendAs(String sendAsEmail) throws IOException {
        service.users().settings().sendAs().delete("me", sendAsEmail).execute();
    }

    /**
     * Gets list of send-as addresses
     *
     * @return list of SendAs or null
     * @throws IOException if ListSendAsResponse could not be retrieved
     */
    public List<SendAs> listSendAs() throws IOException {
        ListSendAsResponse listSendAsResponse = service.users().settings().sendAs().list("me").execute();

        return listSendAsResponse != null ? listSendAsResponse.getSendAs() : null;
    }

    /**
     * Updates a send-as address
     *
     * @param settings the SendAs
     * @return updated SendAs
     * @throws IOException if SendAs could not be updated
     */
    public SendAs updateSendAs(SendAs settings) throws IOException {
        return service.users().settings().sendAs().update("me", settings.getSendAsEmail(), settings).execute();
    }
}
