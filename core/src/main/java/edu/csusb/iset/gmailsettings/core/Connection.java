package edu.csusb.iset.gmailsettings.core;

@SuppressWarnings("unused")
public class Connection {

    private String domain;
    private String credentialsPath;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getCredentialsPath() {
        return credentialsPath;
    }

    public void setCredentialsPath(String credentialsPath) {
        this.credentialsPath = credentialsPath;
    }
}
