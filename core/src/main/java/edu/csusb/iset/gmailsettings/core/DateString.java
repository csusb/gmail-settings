/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.csusb.iset.gmailsettings.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.regex.Pattern;

/**
 * @author Chad Cordero <ccordero@csusb.edu>
 */
@SuppressWarnings("SpellCheckingInspection")
public class DateString {

    private static final Pattern US_MM_DD_YYYY = Pattern.compile("\\d{1,2}/\\d{1,2}/\\d{4}");
    private static final Pattern US_MM_DD_YYYY_HH_MM = Pattern.compile("\\d{1,2}/\\d{1,2}/\\d{4} \\d{2}:\\d{2}");
    private static final Pattern US_MMM_DD_YYYY = Pattern.compile("[a-zA-Z]{3} \\d{1,2}, \\d{4}");
    private static final Pattern US_MMM_DD_YYYY_HH_MM = Pattern.compile("[a-zA-Z]{3} \\d{1,2}, \\d{4} \\d{2}:\\d{2}");
    private static final Pattern US_MMMMM_DD_YYYY = Pattern.compile("[a-zA-Z]{3,9} \\d{1,2}, \\d{4}");
    private static final Pattern US_MMMMM_DD_YYYY_HH_MM = Pattern.compile("[a-zA-Z]{3,9} \\d{1,2}, \\d{4} \\d{2}:\\d{2}");
    private final String date;
    private SimpleDateFormat dateFormat;

    /**
     * Constructs a new DateString
     *
     * @param dateString The date string
     * @param timeZone   The time zone
     */
    public DateString(String dateString, TimeZone timeZone) {
        date = dateString.replace('-', '/');

        if (US_MM_DD_YYYY.matcher(date).matches()) {
            dateFormat = new SimpleDateFormat("MM/dd/yyyy");
            dateFormat.setTimeZone(timeZone);
        } else if (US_MM_DD_YYYY_HH_MM.matcher(date).matches()) {
            dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
            dateFormat.setTimeZone(timeZone);
        } else if (US_MMM_DD_YYYY.matcher(date).matches()) {
            dateFormat = new SimpleDateFormat("MMM dd, yyyy");
            dateFormat.setTimeZone(timeZone);
        } else if (US_MMM_DD_YYYY_HH_MM.matcher(date).matches()) {
            dateFormat = new SimpleDateFormat("MMM dd, yyyy HH:mm");
            dateFormat.setTimeZone(timeZone);
        } else if (US_MMMMM_DD_YYYY.matcher(date).matches()) {
            dateFormat = new SimpleDateFormat("MMMMM dd, yyyy");
            dateFormat.setTimeZone(timeZone);
        } else if (US_MMMMM_DD_YYYY_HH_MM.matcher(date).matches()) {
            dateFormat = new SimpleDateFormat("MMMMM dd, yyyy HH:mm");
            dateFormat.setTimeZone(timeZone);
        }
    }

    /**
     * @return the number of milliseconds since January 1, 1970, 00:00:00 GMT
     * @throws ParseException if the specified date cannot be parsed
     */
    public long parse() throws ParseException {
        return dateFormat.parse(date).getTime();
    }
}
