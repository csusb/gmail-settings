package edu.csusb.iset.gmailsettings.core;

import org.dom4j.*;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class Connections {

    public static final String domainNameAttribute = "domainName";
    public static final String credentialsFileNameAttribute = "credentialsFileName";
    private static final String rootElementName = "connections";
    private static final String connectionElementName = "connection";

    private final Path credentialsPath;
    private final File connectionsFile;

    public Connections() throws IOException {
        // Create directories
        credentialsPath = Constants.APPLICATION_PATH.resolve("Credentials");
        if (!Files.exists(credentialsPath)) {
            Files.createDirectories(credentialsPath);
        }

        // Create config file
        connectionsFile = Constants.APPLICATION_PATH.resolve("connections.xml").toFile();
        if (connectionsFile.createNewFile()) {
            FileWriter writer = new FileWriter(connectionsFile);
            Document document = DocumentHelper.createDocument();
            document.addElement(rootElementName);
            document.write(writer);
            writer.close();
        }
    }

    public void createConnection(String domain, Path source) throws DocumentException, IOException {
        SAXReader reader = new SAXReader();
        Document document = reader.read(connectionsFile);
        if (domain == null) {
            return;
        }

        Node node = document.selectSingleNode(String.format("/%s/%s[@%s = '%s']", rootElementName, connectionElementName, domainNameAttribute, domain));
        if (node == null) {
            //Copy credentials to Credentials directory
            Path target = credentialsPath.resolve(source.getFileName());
            Files.copy(source, target, LinkOption.NOFOLLOW_LINKS);

            //Write config
            Element root = document.getRootElement();
            Element connection = root.addElement(connectionElementName);
            connection.addAttribute(domainNameAttribute, domain);
            connection.addAttribute(credentialsFileNameAttribute, source.getFileName().toString());

            FileWriter writer = new FileWriter(connectionsFile);
            document.write(writer);
            writer.close();
        } else {
            System.err.printf("Connection already defined for %s.\n", domain);
        }
    }

    public void removeConnection(String domain) throws DocumentException, IOException {
        SAXReader reader = new SAXReader();
        Document document = reader.read(connectionsFile);
        if (domain == null) {
            return;
        }

        Node node = document.selectSingleNode(String.format("/%s/%s[@%s = '%s']", rootElementName, connectionElementName, domainNameAttribute, domain));
        if (node != null) {
            Path credentialFilePath = credentialsPath.resolve(node.valueOf(String.format("@%s", credentialsFileNameAttribute)));
            node.detach();

            FileWriter writer = new FileWriter(connectionsFile);
            document.write(writer);
            writer.close();
            Files.delete(credentialFilePath);
        }
    }

    public Path getCredentials(String domain) throws DocumentException, ConnectionNotFoundException {
        SAXReader reader = new SAXReader();
        Document document = reader.read(connectionsFile);

        Node node = document.selectSingleNode(String.format("/%s/%s[@%s = '%s']", rootElementName, connectionElementName, domainNameAttribute, domain));
        if (node == null) {
            throw new ConnectionNotFoundException(String.format("Domain not listed: %s", domain));
        }

        return credentialsPath.resolve(node.valueOf(String.format("@%s", credentialsFileNameAttribute)));
    }

    public List<Connection> list() throws DocumentException {
        List<Connection> connectionList = new ArrayList<>();
        SAXReader reader = new SAXReader();
        Document document = reader.read(connectionsFile);

        List<Node> nodeList = document.selectNodes(String.format("/%s/%s", rootElementName, connectionElementName));
        for (Node node : nodeList) {
            Connection entry = new Connection();
            entry.setDomain(node.valueOf(String.format("@%s", domainNameAttribute)));
            entry.setCredentialsPath(node.valueOf(String.format("@%s", credentialsFileNameAttribute)));

            connectionList.add(entry);
        }

        return connectionList;
    }
}
