package edu.csusb.iset.gmailsettings.core;

/**
 * Signals that a domain is not listed in the connections file
 */
@SuppressWarnings("unused")
public class ConnectionNotFoundException extends Exception {

    public ConnectionNotFoundException() {
        super("Connection not found");
    }

    public ConnectionNotFoundException(String message) {
        super(message);
    }
}
