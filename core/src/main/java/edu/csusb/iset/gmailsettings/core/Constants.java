/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.csusb.iset.gmailsettings.core;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author Chad Cordero <ccordero@csusb.edu>
 */
public class Constants {

    public static final Pattern EMAIL_PATTERN = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    public static final List<String> AUTO_FORWARDING_DISPOSITION = new ArrayList<>(Arrays.asList("archive", "leaveInInbox", "markRead", "trash"));
    public static final List<String> IMAP_EXPUNGE_BEHAVIOR = new ArrayList<>(Arrays.asList("archive", "deleteForever", "trash"));
    public static final List<Integer> IMAP_MAX_FOLDER_SIZE = new ArrayList<>(Arrays.asList(0, 1000, 2000, 5000, 10000));
    public static final List<String> POP_ACCESS_WINDOW = new ArrayList<>(Arrays.asList("allMail", "disabled", "fromNowOn"));
    public static final List<String> POP_DISPOSITION = new ArrayList<>(Arrays.asList("archive", "leaveInInbox", "markRead", "trash"));
    public static final String ACTION_CREATE = "create";
    public static final String ACTION_DELETE = "delete";
    public static final String ACTION_GET = "get";
    public static final String ACTION_LIST = "list";
    public static final String ACTION_UPDATE = "update";
    public static final String COMMAND_AUTO_FORWARDING = "autoForwarding";
    public static final String COMMAND_FORWARDING = "forwarding";
    public static final String COMMAND_IMAP = "imap";
    public static final String COMMAND_POP = "pop";
    public static final String COMMAND_SEND_AS = "sendAs";
    public static final String COMMAND_VACATION = "vacation";
    public static final String OPTION_USER = "user";
    public static final String OPTION_FORWARDING_EMAIL = "forwardingEmail";
    public static final String OPTION_SEND_AS_EMAIL = "sendAsEmail";
    public static final String OPTION_DISPLAY_NAME = "displayName";
    public static final String OPTION_REPLY_TO_ADDRESS = "replyToAddress";
    public static final String OPTION_SIGNATURE = "signature";
    public static final String OPTION_IS_DEFAULT = "isDefault";
    public static final String OPTION_TREAT_AS_ALIAS = "treatAsAlias";
    public static final String OPTION_ENABLED = "enabled";
    public static final String OPTION_EMAIL_ADDRESS = "emailAddress";
    public static final String OPTION_DISPOSITION = "disposition";
    public static final String OPTION_AUTO_EXPUNGE = "autoExpunge";
    public static final String OPTION_EXPUNGE_BEHAVIOR = "expungeBehavior";
    public static final String OPTION_MAX_FOLDER_SIZE = "maxFolderSize";
    public static final String OPTION_ACCESS_WINDOW = "accessWindow";
    public static final String OPTION_ENABLE_AUTO_REPLY = "enableAutoReply";
    public static final String OPTION_RESPONSE_SUBJECT = "responseSubject";
    public static final String OPTION_RESPONSE_BODY_PLAIN_TEXT = "responseBodyPlainText";
    public static final String OPTION_RESPONSE_BODY_HTML = "responseBodyHtml";
    public static final String OPTION_RESTRICT_TO_CONTACTS = "restrictToContacts";
    public static final String OPTION_RESTRICT_TO_DOMAIN = "restrictToDomain";
    public static final String OPTION_START_TIME = "startTime";
    public static final String OPTION_END_TIME = "endTime";
    public static final String APPLICATION_NAME = "GmailSettings";
    public static final String HELP_FOOTER = "\nCopyright(C) 2020, California State University, San Bernardino.\n";
    public static final Path APPLICATION_PATH;

    static {
        String osName = System.getProperty("os.name").toLowerCase();
        if (osName.contains("win")) {
            APPLICATION_PATH = Paths.get(System.getProperty("user.home"), "AppData", "Local", APPLICATION_NAME);
        } else if (osName.contains("mac")) {
            APPLICATION_PATH = Paths.get(System.getProperty("user.home"), "Library", "Application Support", APPLICATION_NAME);
        } else {
            APPLICATION_PATH = Paths.get(System.getProperty("user.home"), "." + APPLICATION_NAME);
        }
    }
}
