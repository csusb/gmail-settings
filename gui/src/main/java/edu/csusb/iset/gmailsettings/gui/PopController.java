package edu.csusb.iset.gmailsettings.gui;

import com.google.api.services.gmail.model.PopSettings;
import edu.csusb.iset.gmailsettings.core.Constants;
import edu.csusb.iset.gmailsettings.core.GmailSettingsService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

@SuppressWarnings("unused")
public class PopController implements Initializable {

    @FXML
    private ComboBox<String> accessWindowComboBox;

    @FXML
    private ComboBox<String> dispositionComboBox;

    @FXML
    private Button saveButton;

    private PopSettings cachedPopSettings;

    private GmailSettingsService cachedService;

    private Logger logger;

    public void setPopSettings(GmailSettingsService service) {
        if (service != null) {
            try {
                cachedPopSettings = service.getPopSettings();
                setComponents(cachedPopSettings);
                cachedService = service;
            } catch (IOException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        } else {
            cachedPopSettings = new PopSettings();
            clearComponents();
        }

        refreshUi(false);
    }

    private void clearComponents() {
        accessWindowComboBox.getSelectionModel().clearSelection();
        dispositionComboBox.getSelectionModel().clearSelection();
    }

    private void setComponents(PopSettings popSettings) {
        accessWindowComboBox.getSelectionModel().select(popSettings.getAccessWindow());
        dispositionComboBox.getSelectionModel().select(popSettings.getDisposition());
    }

    private void refreshUi(boolean hasChanges) {
        if (accessWindowComboBox.getSelectionModel().getSelectedItem().equalsIgnoreCase("disabled")) {
            accessWindowComboBox.setDisable(false);
            dispositionComboBox.setDisable(true);
        } else {
            accessWindowComboBox.setDisable(false);
            dispositionComboBox.setDisable(false);
        }

        saveButton.setDisable(!hasChanges);
    }

    private boolean hasChanges() {
        String selectedAccessWindow = accessWindowComboBox.getSelectionModel().getSelectedItem();
        String selectedDisposition = dispositionComboBox.getSelectionModel().getSelectedItem();

        return !cachedPopSettings.isEmpty() &&
                (!cachedPopSettings.getAccessWindow().equalsIgnoreCase(selectedAccessWindow) ||
                        !cachedPopSettings.getDisposition().equalsIgnoreCase(selectedDisposition));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        accessWindowComboBox.getItems().clear();
        for (String accessWindow : Constants.POP_ACCESS_WINDOW) {
            accessWindowComboBox.getItems().add(accessWindow);
        }
        accessWindowComboBox.getSelectionModel().select("disabled");

        dispositionComboBox.getItems().clear();
        for (String disposition : Constants.POP_DISPOSITION) {
            dispositionComboBox.getItems().add(disposition);
        }
        dispositionComboBox.getSelectionModel().select("leaveInInbox");

        cachedPopSettings = new PopSettings();
        logger = Logger.getLogger(getClass().getName());
    }


    public void accessWindowComboBoxAction(ActionEvent actionEvent) {
        refreshUi(hasChanges());
    }

    public void dispositionComboBoxAction(ActionEvent actionEvent) {
        refreshUi(hasChanges());
    }

    public void saveButtonAction(ActionEvent actionEvent) {
        PopSettings popSettings = new PopSettings();
        if (accessWindowComboBox.getSelectionModel().getSelectedItem().equalsIgnoreCase("disabled")) {
            popSettings.setAccessWindow("disabled");
        } else {
            popSettings.setAccessWindow(accessWindowComboBox.getSelectionModel().getSelectedItem());
            popSettings.setDisposition(dispositionComboBox.getSelectionModel().getSelectedItem());
        }

        try {
            cachedPopSettings = cachedService.updatePopSettings(popSettings);
            setComponents(cachedPopSettings);
            refreshUi(false);
        } catch (IOException e) {
            Alert exceptionAlert = new ExceptionAlert(e);
            logger.log(Level.SEVERE, e.getMessage(), e);
            exceptionAlert.showAndWait();
        }
    }

    public void resetButtonAction(ActionEvent actionEvent) {
        setComponents(cachedPopSettings);
        refreshUi(false);
    }
}
