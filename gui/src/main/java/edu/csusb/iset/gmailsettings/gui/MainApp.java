package edu.csusb.iset.gmailsettings.gui;

import edu.csusb.iset.gmailsettings.core.ApplicationLogger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

@SuppressWarnings("SpellCheckingInspection")
public class MainApp extends Application {

    private static final Properties properties = new Properties();

    public static void main(String[] args) {
        LogManager.getLogManager().addLogger(ApplicationLogger.getLogger(""));
        Logger logger = Logger.getLogger(MainApp.class.getName());

        try {
            properties.load(MainApp.class.getResourceAsStream("project.properties"));
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Could not load properties", e);
        }

        launch(args);
    }

    public static Properties getProperties() {
        return properties;
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("gmailsettings.fxml"));

        Scene scene = new Scene(root);
        scene.getStylesheets().add(getClass().getResource("gmailsettings.css").toExternalForm());
        stage.setTitle(properties.getProperty("title"));
        stage.setScene(scene);
        stage.show();
    }
}
