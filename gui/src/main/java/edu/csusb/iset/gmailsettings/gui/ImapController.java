package edu.csusb.iset.gmailsettings.gui;

import com.google.api.services.gmail.model.ImapSettings;
import edu.csusb.iset.gmailsettings.core.Constants;
import edu.csusb.iset.gmailsettings.core.GmailSettingsService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

@SuppressWarnings("unused")
public class ImapController implements Initializable {

    @FXML
    private CheckBox enabledCheckBox;

    @FXML
    private CheckBox autoExpungeCheckBox;

    @FXML
    private ComboBox<String> expungeBehaviorComboBox;

    @FXML
    private ComboBox<Integer> maxFolderSizeComboBox;

    @FXML
    private Button saveButton;

    private ImapSettings cachedImapSettings;

    private GmailSettingsService cachedService;

    private Logger logger;

    public void setImapSettings(GmailSettingsService service) {
        if (service != null) {
            try {
                cachedImapSettings = service.getImapSettings();
                setComponents(cachedImapSettings);
                cachedService = service;
            } catch (IOException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        } else {
            cachedImapSettings = new ImapSettings();
            clearComponents();
        }

        refreshUi(false);
    }

    private void clearComponents() {
        enabledCheckBox.setSelected(false);
        autoExpungeCheckBox.setSelected(false);
        expungeBehaviorComboBox.getSelectionModel().clearSelection();
        maxFolderSizeComboBox.getSelectionModel().clearSelection();
    }

    private void setComponents(ImapSettings imapSettings) {
        enabledCheckBox.setSelected(imapSettings.getEnabled());
        autoExpungeCheckBox.setSelected(imapSettings.getAutoExpunge());
        expungeBehaviorComboBox.getSelectionModel().select(imapSettings.getExpungeBehavior());
        maxFolderSizeComboBox.getSelectionModel().select(imapSettings.getMaxFolderSize());
    }

    public void enabledCheckBoxAction(ActionEvent actionEvent) {
        refreshUi(hasChanges());
    }

    private void refreshUi(boolean hasChanges) {
        if (enabledCheckBox.isSelected()) {
            autoExpungeCheckBox.setDisable(false);
            expungeBehaviorComboBox.setDisable(autoExpungeCheckBox.isSelected());
            maxFolderSizeComboBox.setDisable(false);
        } else {
            autoExpungeCheckBox.setDisable(true);
            expungeBehaviorComboBox.setDisable(true);
            maxFolderSizeComboBox.setDisable(true);
        }

        saveButton.setDisable(!hasChanges);
    }

    public void autoExpungeCheckBoxAction(ActionEvent actionEvent) {
        refreshUi(hasChanges());
    }

    public boolean hasChanges() {
        String selectedExpungeBehavior = expungeBehaviorComboBox.getSelectionModel().getSelectedItem();
        Integer selectedMaxFolderSize = maxFolderSizeComboBox.getSelectionModel().getSelectedItem();

        return !cachedImapSettings.isEmpty() &&
                (cachedImapSettings.getEnabled().compareTo(enabledCheckBox.isSelected()) != 0 ||
                        cachedImapSettings.getAutoExpunge().compareTo(autoExpungeCheckBox.isSelected()) != 0 ||
                        !cachedImapSettings.getExpungeBehavior().equalsIgnoreCase(selectedExpungeBehavior) ||
                        cachedImapSettings.getMaxFolderSize().compareTo(selectedMaxFolderSize) != 0);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        expungeBehaviorComboBox.getItems().clear();
        for (String expungeBehavior : Constants.IMAP_EXPUNGE_BEHAVIOR) {
            expungeBehaviorComboBox.getItems().add(expungeBehavior);
        }
        expungeBehaviorComboBox.getSelectionModel().select("archive");

        maxFolderSizeComboBox.getItems().clear();
        for (Integer maxFolderSize : Constants.IMAP_MAX_FOLDER_SIZE) {
            maxFolderSizeComboBox.getItems().add(maxFolderSize);
        }
        maxFolderSizeComboBox.getSelectionModel().select(Integer.valueOf(0));

        cachedImapSettings = new ImapSettings();
        logger = Logger.getLogger(getClass().getName());
    }

    public void expungeBehaviorComboBoxAction(ActionEvent actionEvent) {
        refreshUi(hasChanges());
    }

    public void maxFolderSizeComboBoxAction(ActionEvent actionEvent) {
        refreshUi(hasChanges());
    }

    public void saveButtonAction(ActionEvent actionEvent) {
        ImapSettings imapSettings = new ImapSettings();
        if (enabledCheckBox.isSelected()) {
            imapSettings.setEnabled(true);
            if (autoExpungeCheckBox.isSelected()) {
                imapSettings.setAutoExpunge(true);
            } else {
                imapSettings.setAutoExpunge(false);
                imapSettings.setExpungeBehavior(expungeBehaviorComboBox.getSelectionModel().getSelectedItem());
            }
            imapSettings.setMaxFolderSize(maxFolderSizeComboBox.getSelectionModel().getSelectedItem());
        } else {
            imapSettings.setEnabled(false);
        }

        try {
            cachedImapSettings = cachedService.updateImapSettings(imapSettings);
            setComponents(cachedImapSettings);
            refreshUi(false);
        } catch (IOException e) {
            Alert exceptionAlert = new ExceptionAlert(e);
            logger.log(Level.SEVERE, e.getMessage(), e);
            exceptionAlert.showAndWait();
        }
    }

    public void resetButtonAction(ActionEvent actionEvent) {
        setComponents(cachedImapSettings);
        refreshUi(false);
    }
}
