package edu.csusb.iset.gmailsettings.gui;

import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import org.apache.commons.lang3.exception.ExceptionUtils;

public class ExceptionAlert extends Alert {

    private final Label exceptionLabel = new Label();
    private final TextArea exceptionTextArea = new TextArea();

    public ExceptionAlert(Exception ex) {
        super(AlertType.ERROR);
        exceptionLabel.setText("Stacktrace:");
        exceptionTextArea.setText(ExceptionUtils.getStackTrace(ex));
        exceptionTextArea.setEditable(false);
        exceptionTextArea.setWrapText(true);

        setTitle("Exception");
        setHeaderText(String.format("Exception in %s", ex.getStackTrace()[0].getClassName()));
        setContentText(ex.getMessage());
        getDialogPane().setExpandableContent(getGridPane());
    }

    private GridPane getGridPane() {
        GridPane.setVgrow(exceptionTextArea, Priority.ALWAYS);
        GridPane.setHgrow(exceptionTextArea, Priority.ALWAYS);

        GridPane gridPane = new GridPane();
        gridPane.setMaxWidth(Double.MAX_VALUE);
        gridPane.add(exceptionLabel, 0, 0);
        gridPane.add(exceptionTextArea, 0, 1);

        return gridPane;
    }
}
