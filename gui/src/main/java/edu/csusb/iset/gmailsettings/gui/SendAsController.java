package edu.csusb.iset.gmailsettings.gui;

import com.google.api.services.gmail.model.SendAs;
import edu.csusb.iset.gmailsettings.core.GmailSettingsService;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

@SuppressWarnings("unused")
public class SendAsController implements Initializable {

    @FXML
    private TableView<SendAs> sendAsTableView;

    @FXML
    private TableColumn<SendAs, String> sendAsEmailTableColumn;

    @FXML
    private TableColumn<SendAs, String> displayNameTableColumn;

    @FXML
    private TableColumn<SendAs, String> replyToAddressTableColumn;

    @FXML
    private TableColumn<SendAs, Boolean> isDefaultTableColumn;

    @FXML
    private TableColumn<SendAs, Boolean> treatAsAliasTableColumn;

    @FXML
    private TableColumn<SendAs, String> verificationStatusTableColumn;

    @FXML
    private Button editButton;

    @FXML
    private Button deleteButton;

    private GmailSettingsService cachedService;

    private Logger logger;

    public void editButtonAction(ActionEvent actionEvent) {
        SendAs selectedSendAs = sendAsTableView.getSelectionModel().getSelectedItem();
        int selectedIndex = sendAsTableView.getSelectionModel().getSelectedIndex();
        SendAsDialog dialog = new SendAsDialog();
        dialog.setSendAs(selectedSendAs);

        Optional<SendAs> result = dialog.showAndWait();
        if (result.isPresent()) {
            try {
                cachedService.updateSendAs(result.get());
                sendAsTableView.getItems().set(selectedIndex, result.get());
            } catch (IOException e) {
                Alert exceptionAlert = new ExceptionAlert(e);
                logger.log(Level.SEVERE, e.getMessage(), e);
                exceptionAlert.showAndWait();
            }
        }
    }

    public void deleteButtonAction(ActionEvent actionEvent) {
        try {
            SendAs selectedSendAs = sendAsTableView.getSelectionModel().getSelectedItem();
            int selectedIndex = sendAsTableView.getSelectionModel().getSelectedIndex();

            cachedService.deleteSendAs(selectedSendAs.getSendAsEmail());
            sendAsTableView.getItems().remove(selectedIndex);
        } catch (IOException e) {
            Alert exceptionAlert = new ExceptionAlert(e);
            logger.log(Level.SEVERE, e.getMessage(), e);
            exceptionAlert.showAndWait();
        }
    }

    public void createButtonAction(ActionEvent actionEvent) {
        Dialog<SendAs> dialog = new SendAsDialog();

        Optional<SendAs> result = dialog.showAndWait();
        if (result.isPresent()) {
            try {
                cachedService.createSendAs(result.get());
                sendAsTableView.getItems().add(result.get());
            } catch (IOException e) {
                Alert exceptionAlert = new ExceptionAlert(e);
                logger.log(Level.SEVERE, e.getMessage(), e);
                exceptionAlert.showAndWait();
            }
        }
    }

    public void setSendAsEmail(GmailSettingsService service) {
        sendAsTableView.getItems().clear();
        try {
            List<SendAs> sendAsList = service.listSendAs();
            sendAsTableView.getItems().addAll(sendAsList);
            cachedService = service;
        } catch (IOException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        sendAsTableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                Optional<Boolean> isPrimary = Optional.ofNullable(newSelection.getIsPrimary());

                deleteButton.setDisable(isPrimary.isPresent() && isPrimary.get());
                editButton.setDisable(false);
            } else {
                deleteButton.setDisable(true);
                editButton.setDisable(true);
            }
        });
        sendAsEmailTableColumn.setCellValueFactory(new PropertyValueFactory<>("sendAsEmail"));
        sendAsEmailTableColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        displayNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("displayName"));
        displayNameTableColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        replyToAddressTableColumn.setCellValueFactory(new PropertyValueFactory<>("replyToAddress"));
        replyToAddressTableColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        isDefaultTableColumn.setCellValueFactory(param -> {
            SendAs sendAs = param.getValue();

            SimpleBooleanProperty booleanProperty = new SimpleBooleanProperty(sendAs.getIsDefault());
            booleanProperty.addListener((observable, oldValue, newValue) -> sendAs.setIsDefault(newValue));

            return booleanProperty;
        });
        isDefaultTableColumn.setCellFactory(param -> new CheckBoxTableCell<>());
        treatAsAliasTableColumn.setCellValueFactory(param -> {
            SendAs sendAs = param.getValue();

            SimpleBooleanProperty booleanProperty = new SimpleBooleanProperty(Boolean.TRUE.equals(sendAs.getTreatAsAlias()));
            booleanProperty.addListener((observable, oldValue, newValue) -> sendAs.setTreatAsAlias(newValue));

            return booleanProperty;
        });
        treatAsAliasTableColumn.setCellFactory(param -> new CheckBoxTableCell<>());
        verificationStatusTableColumn.setCellValueFactory(new PropertyValueFactory<>("verificationStatus"));
        verificationStatusTableColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        logger = Logger.getLogger(getClass().getName());
    }
}
