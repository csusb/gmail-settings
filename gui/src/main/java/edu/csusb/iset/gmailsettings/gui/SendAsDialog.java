package edu.csusb.iset.gmailsettings.gui;

import com.google.api.services.gmail.model.SendAs;
import edu.csusb.iset.gmailsettings.core.Constants;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;

import java.util.Optional;

public class SendAsDialog extends Dialog<SendAs> {

    private final TextField sendAsEmailTextField = new TextField();
    private final TextField displayNameTextField = new TextField();
    private final TextField replyToAddressTextField = new TextField();
    private final CheckBox isDefaultCheckBox = new CheckBox();
    private final CheckBox treatAsAliasCheckBox = new CheckBox();

    public SendAsDialog() {
        ButtonType okButtonType = ButtonType.OK;
        sendAsEmailTextField.setPrefWidth(225);
        isDefaultCheckBox.setText("Default");
        treatAsAliasCheckBox.setText("Treat as alias");

        setTitle("Send-as Address");
        getDialogPane().setContent(getGridPane());
        getDialogPane().getButtonTypes().addAll(okButtonType, ButtonType.CANCEL);

        Node okButton = getDialogPane().lookupButton(okButtonType);
        okButton.setDisable(true);
        sendAsEmailTextField.textProperty().addListener(((observable, oldValue, newValue) -> okButton.setDisable(!Constants.EMAIL_PATTERN.matcher(newValue).matches())));

        setResultConverter(dialogButton -> {
            if (dialogButton == okButtonType) {
                return getSendAs();
            } else {
                return null;
            }
        });
    }

    private SendAs getSendAs() {
        SendAs sendAs = new SendAs();
        sendAs.setSendAsEmail(sendAsEmailTextField.getText());
        sendAs.setDisplayName(displayNameTextField.getText());
        sendAs.setReplyToAddress(replyToAddressTextField.getText());
        sendAs.setIsDefault(isDefaultCheckBox.isSelected());
        sendAs.setTreatAsAlias(treatAsAliasCheckBox.isSelected());

        return sendAs;
    }

    public void setSendAs(SendAs sendAs) {
        if (sendAs != null) {
            Optional<Boolean> treatAsAlias = Optional.ofNullable(sendAs.getTreatAsAlias());
            Optional<Boolean> isPrimary = Optional.ofNullable(sendAs.getIsPrimary());

            sendAsEmailTextField.setText(sendAs.getSendAsEmail());
            displayNameTextField.setText(sendAs.getDisplayName());
            replyToAddressTextField.setText(sendAs.getReplyToAddress());
            isDefaultCheckBox.setSelected(sendAs.getIsDefault());
            treatAsAliasCheckBox.setSelected(treatAsAlias.orElse(false));

            sendAsEmailTextField.setDisable(true);
            if (isPrimary.orElse(false)) {
                displayNameTextField.setDisable(true);
                treatAsAliasCheckBox.setDisable(true);
            } else {
                displayNameTextField.setDisable(false);
                treatAsAliasCheckBox.setDisable(false);
            }
        }
    }

    private GridPane getGridPane() {
        GridPane gridPane = new GridPane();
        gridPane.setHgap(5);
        gridPane.setVgap(5);
        gridPane.setPadding(new Insets(5, 5, 5, 5));

        ColumnConstraints col0 = new ColumnConstraints();
        col0.setHalignment(HPos.RIGHT);
        gridPane.getColumnConstraints().setAll(col0, new ColumnConstraints());

        gridPane.add(new Label("Send-as email"), 0, 0);
        gridPane.add(sendAsEmailTextField, 1, 0);
        gridPane.add(new Label("Display name"), 0, 1);
        gridPane.add(displayNameTextField, 1, 1);
        gridPane.add(new Label("Reply-to address (Optional)"), 0, 2);
        gridPane.add(replyToAddressTextField, 1, 2);
        gridPane.add(isDefaultCheckBox, 1, 3);
        gridPane.add(treatAsAliasCheckBox, 1, 4);

        return gridPane;
    }
}
