package edu.csusb.iset.gmailsettings.gui;

import edu.csusb.iset.gmailsettings.core.ConnectionNotFoundException;
import edu.csusb.iset.gmailsettings.core.GmailSettingsService;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.DocumentException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class GmailSettingsController implements Initializable {

    private final Clipboard clipboard = Clipboard.getSystemClipboard();
    private final ClipboardContent clipboardContent = new ClipboardContent();
    @FXML
    private MenuBar menuBar;
    @FXML
    private MenuItem fileCloseMenuItem;
    @FXML
    private MenuItem editCutMenuItem;
    @FXML
    private MenuItem editCopyMenuItem;
    @FXML
    private MenuItem editPasteMenuItem;
    @FXML
    private Tab autoForwardingTab;
    @FXML
    private AutoForwardingController autoForwardingPaneController;
    @FXML
    private Tab imapTab;
    @FXML
    private ImapController imapPaneController;
    @FXML
    private Tab popTab;
    @FXML
    private PopController popPaneController;
    @FXML
    private Tab sendAsTab;
    @FXML
    private SendAsController sendAsPaneController;
    @FXML
    private Tab vacationTab;
    @FXML
    private VacationController vacationPaneController;
    @FXML
    private TextField userTextField;
    @FXML
    private Label connectedLabel;

    private Logger logger;

    public void searchButtonAction(ActionEvent actionEvent) {
        String userName = userTextField.getText();
        connectedLabel.setText("Not connected");
        autoForwardingTab.setDisable(true);
        imapTab.setDisable(true);
        popTab.setDisable(true);
        sendAsTab.setDisable(true);
        vacationTab.setDisable(true);

        try {
            GmailSettingsService service = new GmailSettingsService(userName);
            loadAutoForwarding(service);
            loadImapSettings(service);
            loadPopSettings(service);
            loadSendAsAddress(service);
            loadVacationSettings(service);

            connectedLabel.setText("Connected");
            autoForwardingTab.setDisable(false);
            imapTab.setDisable(false);
            popTab.setDisable(false);
            sendAsTab.setDisable(false);
            vacationTab.setDisable(false);
        } catch (IOException | DocumentException e) {
            Alert exceptionAlert = new ExceptionAlert(e);
            logger.log(Level.SEVERE, e.getMessage(), e);
            exceptionAlert.showAndWait();
        } catch (ConnectionNotFoundException e) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Connection Warning");
            alert.setHeaderText("Connection not found");
            alert.setContentText("Connection could not be found for the specified user's domain. If you have the service account credentials file, you can add it. Just go to:\n\nFile > Connections...");
            alert.showAndWait();
        }
    }

    private void loadAutoForwarding(GmailSettingsService service) {
        autoForwardingPaneController.setAutoForwarding(service);
    }

    private void loadImapSettings(GmailSettingsService service) {
        imapPaneController.setImapSettings(service);
    }

    private void loadPopSettings(GmailSettingsService service) {
        popPaneController.setPopSettings(service);
    }

    private void loadSendAsAddress(GmailSettingsService service) {
        sendAsPaneController.setSendAsEmail(service);
    }

    private void loadVacationSettings(GmailSettingsService service) {
        vacationPaneController.setVacationSettings(service);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        String OS = System.getProperty("os.name").toLowerCase();
        if (OS.contains("mac")) {
            menuBar.setUseSystemMenuBar(true);
            fileCloseMenuItem.setVisible(false);
        } else {
            fileCloseMenuItem.setVisible(true);
        }

        logger = Logger.getLogger(getClass().getName());
    }

    public void fileCloseMenuItemAction(ActionEvent actionEvent) {
        Platform.exit();
    }

    public void helpAboutMenuItemAction(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("about.fxml"));
            Parent root = loader.load();

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("About Gmail Settings");
            alert.setHeaderText(null);
            alert.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("Images/GmailSettings128.png"))));
            alert.getDialogPane().setContent(root);
            alert.showAndWait();
        } catch (IOException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public void fileConnectionsMenuItemAction(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("connections.fxml"));
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("API Connections");
        stage.setScene(new Scene(root));
        stage.show();
    }

    public void editCutMenuItemAction(ActionEvent actionEvent) {
        if (userTextField.isFocused()) {
            String text = userTextField.getSelectedText();
            if (StringUtils.isBlank(text)) {
                return;
            }

            clipboardContent.putString(text);
            clipboard.setContent(clipboardContent);
            userTextField.replaceSelection("");
        } else if (vacationPaneController.isFocused()) {
            String text = vacationPaneController.getSelectedText();
            if (StringUtils.isBlank(text)) {
                return;
            }

            clipboardContent.putString(text);
            clipboard.setContent(clipboardContent);
            vacationPaneController.replaceSelection("");
        } else {
            java.awt.Toolkit.getDefaultToolkit().beep();
        }
    }

    public void editCopyMenuItemAction(ActionEvent actionEvent) {
        if (userTextField.isFocused()) {
            String text = userTextField.getSelectedText();
            if (StringUtils.isBlank(text)) {
                return;
            }

            clipboardContent.putString(text);
            clipboard.setContent(clipboardContent);
        } else if (vacationPaneController.isFocused()) {
            String text = vacationPaneController.getSelectedText();
            if (StringUtils.isBlank(text)) {
                return;
            }

            clipboardContent.putString(text);
            clipboard.setContent(clipboardContent);
        } else {
            java.awt.Toolkit.getDefaultToolkit().beep();
        }
    }

    public void editPasteMenuItemAction(ActionEvent actionEvent) {
        if (userTextField.isFocused()) {
            String text = clipboard.getString();
            if (StringUtils.isBlank(text)) {
                return;
            }

            userTextField.replaceSelection(text);
        } else if (vacationPaneController.isFocused()) {
            String text = clipboard.getString();
            if (StringUtils.isBlank(text)) {
                return;
            }

            vacationPaneController.replaceSelection(text);
        } else {
            java.awt.Toolkit.getDefaultToolkit().beep();
        }
    }

    public void fileInstallCommandLineTools(ActionEvent actionEvent) {
        Properties properties = MainApp.getProperties();
        String nameProperty = properties.getProperty("name");
        String versionProperty = properties.getProperty("version");

        Path archiveFile = Paths.get("/tmp", String.format("%s-%s.tar", nameProperty, versionProperty));
        Path localDir = Paths.get("/usr", "local");
        Path cliToolsDir = localDir.resolve(String.format("%s-%s", nameProperty, versionProperty));
        Path linkFile = localDir.resolve(nameProperty);
        Path binDir = linkFile.resolve("bin");
        Path pathsFile = Paths.get("/etc", "paths.d", String.format("%sCLI", nameProperty));

        String script = String.format("tar xvf %s --directory %s; " +
                "ln -s %s %s; " +
                "echo \\\"%s\\\" > %s",
                archiveFile.toString(), localDir.toString(),
                cliToolsDir.toString(), linkFile.toString(),
                binDir.toString(), pathsFile.toString());
        String prompt = "Gmail Settings requires administrator privileges to install command-line tools.";
        String command = String.format("do shell script \"%s\" with prompt \"%s\" with administrator privileges", script, prompt);

        String[] shellCommand = {"osascript", "-e", command};
        InputStream inputStream = getClass().getResourceAsStream(archiveFile.getFileName().toString());

        try {
            if (!Files.exists(archiveFile)) {
                Files.copy(inputStream, archiveFile);
            }

            Process process = new ProcessBuilder(shellCommand).start();
            process.waitFor();
            Files.delete(archiveFile);

            if (process.exitValue() != 0) {
                InputStream errorStream = process.getErrorStream();
                String contentText = new BufferedReader(new InputStreamReader(errorStream)).lines().collect(Collectors.joining("\n"));

                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Script exited with non-zero status.");
                alert.setContentText(contentText);
                alert.showAndWait();
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Exception copying extract file", e);
        } catch (InterruptedException e) {
            logger.log(Level.SEVERE, "Process interrupted", e);
        }
    }
}
