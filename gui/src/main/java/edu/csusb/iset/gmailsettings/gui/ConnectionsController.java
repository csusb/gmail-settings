package edu.csusb.iset.gmailsettings.gui;

import edu.csusb.iset.gmailsettings.core.Connection;
import edu.csusb.iset.gmailsettings.core.Connections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.dom4j.DocumentException;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

@SuppressWarnings("unused")
public class ConnectionsController implements Initializable {
    @FXML
    private TextField domainNameTextField;

    @FXML
    private Button importButton;

    @FXML
    private TableView<Connection> connectionTableView;

    @FXML
    private TableColumn<Connection, String> domainNameTableColumn;

    @FXML
    private TableColumn<Connection, String> credentialsTableColumn;

    @FXML
    private Button okButton;

    private Logger logger;

    public void okButtonAction(ActionEvent actionEvent) {
        Stage stage = (Stage) okButton.getScene().getWindow();
        stage.close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        domainNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("domain"));
        credentialsTableColumn.setCellValueFactory(new PropertyValueFactory<>("credentialsPath"));
        logger = Logger.getLogger(getClass().getName());

        try {
            Connections connections = new Connections();
            connectionTableView.getItems().clear();

            List<Connection> connectionList = connections.list();
            for (Connection connection : connectionList) {
                connectionTableView.getItems().add(connection);
            }
        } catch (IOException | DocumentException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public void importButtonAction(ActionEvent actionEvent) {
        String domainName = domainNameTextField.getText();
        if (domainName.isEmpty()) {
            return;
        }

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Credentials File");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Credentials Files", "*.json"));

        File credentialsFile = fileChooser.showOpenDialog(importButton.getScene().getWindow());
        if (credentialsFile != null) {
            try {
                Connections connections = new Connections();
                connections.createConnection(domainName, credentialsFile.toPath());

                Connection connection = new Connection();
                connection.setDomain(domainName);
                connection.setCredentialsPath(credentialsFile.getName());
                connectionTableView.getItems().add(connection);
            } catch (IOException | DocumentException e) {
                Alert exceptionAlert = new ExceptionAlert(e);
                logger.log(Level.SEVERE, e.getMessage(), e);
                exceptionAlert.showAndWait();
            }
        }
    }

    public void removeButtonAction(ActionEvent actionEvent) {
        Connection connection = connectionTableView.getSelectionModel().getSelectedItem();
        if (connection != null) {
            try {
                Connections connections = new Connections();
                connections.removeConnection(connection.getDomain());
                connectionTableView.getItems().remove(connection);
            } catch (IOException | DocumentException e) {
                Alert exceptionAlert = new ExceptionAlert(e);
                logger.log(Level.SEVERE, e.getMessage(), e);
                exceptionAlert.showAndWait();
            }
        }
    }
}
