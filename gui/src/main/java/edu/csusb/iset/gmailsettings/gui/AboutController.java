package edu.csusb.iset.gmailsettings.gui;

import edu.csusb.iset.gmailsettings.core.Constants;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

public class AboutController implements Initializable {

    @FXML
    private Label titleLabel;

    @FXML
    private Label versionLabel;

    @FXML
    private Label javaFxVersionLabel;

    @FXML
    private Label gmailApiVersionLabel;

    @FXML
    private Label copyrightLabel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Properties properties = MainApp.getProperties();

        titleLabel.setText(properties.getProperty("title"));
        versionLabel.setText(String.format("Version: %s", properties.get("version")));
        javaFxVersionLabel.setText(String.format("JavaFX Version: %s", properties.get("javafx-version")));
        gmailApiVersionLabel.setText(String.format("Gmail API Version: %s", properties.get("gmail-api-version")));
        copyrightLabel.setText(Constants.HELP_FOOTER);
    }
}
