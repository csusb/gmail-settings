package edu.csusb.iset.gmailsettings.gui;

import com.google.api.services.gmail.model.VacationSettings;
import edu.csusb.iset.gmailsettings.core.GmailSettingsService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.web.HTMLEditor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

@SuppressWarnings("unused")
public class VacationController implements Initializable {

    @FXML
    private CheckBox enableAutoReplyCheckBox;

    @FXML
    private TextField responseSubjectTextField;

    @FXML
    private RadioButton responseBodyPlainTextRadioButton;

    @FXML
    private RadioButton responseBodyHtmlRadioButton;

    @FXML
    private TextArea responseBodyPlainTextArea;

    @FXML
    private HTMLEditor responseBodyHtmlEditor;

    @FXML
    private CheckBox restrictToContactsCheckBox;

    @FXML
    private CheckBox restrictToDomainCheckBox;

    @FXML
    private DatePicker startTimeDatePicker;

    @FXML
    private DatePicker endTimeDatePicker;

    @FXML
    private Button saveButton;

    private VacationSettings cachedVacationSettings;

    private GmailSettingsService cachedService;

    private Logger logger;

    public boolean isFocused() {
        return (responseSubjectTextField.isFocused() ||
                responseBodyPlainTextArea.isFocused());
    }

    public String getSelectedText() {
        if (responseSubjectTextField.isFocused()) {
            return responseSubjectTextField.getSelectedText();
        } else if (responseBodyPlainTextArea.isFocused()) {
            return responseBodyPlainTextArea.getSelectedText();
        } else {
            return null;
        }
    }

    public void replaceSelection(String replacement) {
        if (responseSubjectTextField.isFocused()) {
            responseSubjectTextField.replaceSelection(replacement);
        } else if (responseBodyPlainTextArea.isFocused()) {
            responseBodyPlainTextArea.replaceSelection(replacement);
        }
    }

    public void setVacationSettings(GmailSettingsService service) {
        if (service != null) {
            try {
                cachedVacationSettings = service.getVacationSettings();
                setComponents(cachedVacationSettings);
                cachedService = service;
            } catch (IOException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        } else {
            cachedVacationSettings = new VacationSettings();
            clearComponents();
        }

        refreshUi(false);
    }

    private LocalDate getLocalDate(Long date) {
        if (date == null) {
            return null;
        } else {
            return new Date(date).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        }
    }

    private void setComponents(VacationSettings vacationSettings) {
        Optional<String> responseBodyPlainText = Optional.ofNullable(vacationSettings.getResponseBodyPlainText());
        Optional<String> responseBodyHtml = Optional.ofNullable(vacationSettings.getResponseBodyHtml());

        enableAutoReplyCheckBox.setSelected(vacationSettings.getEnableAutoReply());
        responseSubjectTextField.setText(vacationSettings.getResponseSubject());
        if (responseBodyPlainText.isPresent()) {
            responseBodyPlainTextRadioButton.setSelected(true);
            responseBodyHtmlRadioButton.setSelected(false);
            responseBodyPlainTextArea.setText(responseBodyPlainText.get());
        } else if (responseBodyHtml.isPresent()) {
            responseBodyPlainTextRadioButton.setSelected(false);
            responseBodyHtmlRadioButton.setSelected(true);
            responseBodyHtmlEditor.setHtmlText(responseBodyHtml.get());
        }
        restrictToContactsCheckBox.setSelected(vacationSettings.getRestrictToContacts());
        restrictToDomainCheckBox.setSelected(vacationSettings.getRestrictToDomain());
        startTimeDatePicker.setValue(getLocalDate(vacationSettings.getStartTime()));
        endTimeDatePicker.setValue(getLocalDate(vacationSettings.getEndTime()));
    }

    private void clearComponents() {
        enableAutoReplyCheckBox.setSelected(false);
        responseSubjectTextField.clear();
        responseBodyPlainTextRadioButton.setSelected(true);
        responseBodyPlainTextArea.clear();
        responseBodyHtmlRadioButton.setSelected(false);
        responseBodyHtmlEditor.setHtmlText("");
        restrictToContactsCheckBox.setSelected(false);
        restrictToDomainCheckBox.setSelected(false);
        startTimeDatePicker.setValue(null);
        endTimeDatePicker.setValue(null);
    }

    public void enableAutoReplyCheckBoxAction(ActionEvent actionEvent) {
        refreshUi(hasChanges());
    }

    private void refreshUi(boolean hasChanges) {
        if (enableAutoReplyCheckBox.isSelected()) {
            responseSubjectTextField.setDisable(false);
            responseBodyPlainTextRadioButton.setDisable(false);
            responseBodyHtmlRadioButton.setDisable(false);
            if (responseBodyPlainTextRadioButton.isSelected()) {
                responseBodyPlainTextArea.setVisible(true);
                responseBodyPlainTextArea.setDisable(false);
                responseBodyHtmlEditor.setVisible(false);
                responseBodyHtmlEditor.setDisable(true);
            } else if (responseBodyHtmlRadioButton.isSelected()) {
                responseBodyPlainTextArea.setVisible(false);
                responseBodyPlainTextArea.setDisable(true);
                responseBodyHtmlEditor.setVisible(true);
                responseBodyHtmlEditor.setDisable(false);
            }
            restrictToContactsCheckBox.setDisable(false);
            restrictToDomainCheckBox.setDisable(false);
            startTimeDatePicker.setDisable(false);
            endTimeDatePicker.setDisable(false);
        } else {
            responseSubjectTextField.setDisable(true);
            responseBodyPlainTextRadioButton.setDisable(true);
            responseBodyHtmlRadioButton.setDisable(true);
            if (responseBodyPlainTextRadioButton.isSelected()) {
                responseBodyPlainTextArea.setVisible(true);
                responseBodyPlainTextArea.setDisable(true);
                responseBodyHtmlEditor.setVisible(false);
                responseBodyHtmlEditor.setDisable(true);
            } else if (responseBodyHtmlRadioButton.isSelected()) {
                responseBodyPlainTextArea.setVisible(false);
                responseBodyPlainTextArea.setDisable(true);
                responseBodyHtmlEditor.setVisible(true);
                responseBodyHtmlEditor.setDisable(true);
            }
            restrictToContactsCheckBox.setDisable(true);
            restrictToDomainCheckBox.setDisable(true);
            startTimeDatePicker.setDisable(true);
            endTimeDatePicker.setDisable(true);
        }

        saveButton.setDisable(!hasChanges);
    }

    public void responseBodyPlainTextRadioButtonAction(ActionEvent actionEvent) {
        refreshUi(hasChanges());
    }

    public void responseBodyHtmlRadioButtonAction(ActionEvent actionEvent) {
        refreshUi(hasChanges());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        clearComponents();
        cachedVacationSettings = new VacationSettings();
        logger = Logger.getLogger(getClass().getName());
    }

    private boolean hasChanges() {
        Long startTime = startTimeDatePicker.getValue() != null ? startTimeDatePicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli() : null;
        Long endTime = endTimeDatePicker.getValue() != null ? endTimeDatePicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli() : null;
        String bodyHtml = Jsoup.parse(responseBodyHtmlEditor.getHtmlText()).outputSettings(new Document.OutputSettings().prettyPrint(false)).body().html();

        return !cachedVacationSettings.isEmpty() &&
                (cachedVacationSettings.getEnableAutoReply().compareTo(enableAutoReplyCheckBox.isSelected()) != 0) ||
                !cachedVacationSettings.getResponseSubject().equalsIgnoreCase(responseSubjectTextField.getText()) ||
                !Optional.ofNullable(cachedVacationSettings.getResponseBodyPlainText()).orElse("").equals(responseBodyPlainTextArea.getText()) ||
                !Optional.ofNullable(cachedVacationSettings.getResponseBodyHtml()).orElse("").equals(bodyHtml) ||
                cachedVacationSettings.getRestrictToContacts().compareTo(restrictToContactsCheckBox.isSelected()) != 0 ||
                cachedVacationSettings.getRestrictToDomain().compareTo(restrictToDomainCheckBox.isSelected()) != 0 ||
                !Optional.ofNullable(cachedVacationSettings.getStartTime()).equals(Optional.ofNullable(startTime)) ||
                !Optional.ofNullable(cachedVacationSettings.getEndTime()).equals(Optional.ofNullable(endTime));
    }

    public void responseSubjectTextFieldAction(ActionEvent actionEvent) {
        refreshUi(hasChanges());
    }

    public void restrictToContactsCheckBoxAction(ActionEvent actionEvent) {
        refreshUi(hasChanges());
    }

    public void restrictToDomainCheckBoxAction(ActionEvent actionEvent) {
        refreshUi(hasChanges());
    }

    public void startTimeDatePickerAction(ActionEvent actionEvent) {
        refreshUi(hasChanges());
    }

    public void endTimeDatePickerAction(ActionEvent actionEvent) {
        refreshUi(hasChanges());
    }

    public void responseBodyPlainTextAreaAction(KeyEvent keyEvent) {
        refreshUi(hasChanges());
    }

    public void responseBodyHtmlEditorAction(KeyEvent keyEvent) {
        refreshUi(hasChanges());
    }

    public void saveButtonAction(ActionEvent actionEvent) {
        VacationSettings vacationSettings = new VacationSettings();
        vacationSettings.setEnableAutoReply(enableAutoReplyCheckBox.isSelected());
        vacationSettings.setResponseSubject(responseSubjectTextField.getText());
        if (responseBodyPlainTextRadioButton.isSelected()) {
            vacationSettings.setResponseBodyPlainText(responseBodyPlainTextArea.getText());
        } else if (responseBodyHtmlRadioButton.isSelected()) {
            String bodyHtml = Jsoup.parse(responseBodyHtmlEditor.getHtmlText()).outputSettings(new Document.OutputSettings().prettyPrint(false)).body().html();
            vacationSettings.setResponseBodyHtml(bodyHtml);
        }
        vacationSettings.setRestrictToContacts(restrictToContactsCheckBox.isSelected());
        vacationSettings.setRestrictToDomain(restrictToDomainCheckBox.isSelected());
        if (startTimeDatePicker.getValue() != null)
            vacationSettings.setStartTime(startTimeDatePicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli());
        if (endTimeDatePicker.getValue() != null)
            vacationSettings.setEndTime(endTimeDatePicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli());

        try {
            cachedVacationSettings = cachedService.updateVacationSettings(vacationSettings);
            setComponents(cachedVacationSettings);
            refreshUi(false);
        } catch (IOException e) {
            Alert exceptionAlert = new ExceptionAlert(e);
            logger.log(Level.SEVERE, e.getMessage(), e);
            exceptionAlert.showAndWait();
        }
    }

    public void resetButtonAction(ActionEvent actionEvent) {
        setComponents(cachedVacationSettings);
        refreshUi(false);
    }
}
