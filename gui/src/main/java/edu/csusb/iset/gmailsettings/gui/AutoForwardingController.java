package edu.csusb.iset.gmailsettings.gui;

import com.google.api.services.gmail.model.AutoForwarding;
import com.google.api.services.gmail.model.ForwardingAddress;
import edu.csusb.iset.gmailsettings.core.Constants;
import edu.csusb.iset.gmailsettings.core.GmailSettingsService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

@SuppressWarnings("unused")
public class AutoForwardingController implements Initializable {

    @FXML
    private CheckBox enabledCheckBox;

    @FXML
    private ComboBox<String> emailAddressComboBox;

    @FXML
    private ComboBox<String> dispositionComboBox;

    @FXML
    private Button deleteButton;

    @FXML
    private Button saveButton;

    @FXML
    private Button resetButton;

    private AutoForwarding cachedAutoForwarding;

    private GmailSettingsService cachedService;

    private Logger logger;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        dispositionComboBox.getItems().clear();
        for (String disposition : Constants.AUTO_FORWARDING_DISPOSITION) {
            dispositionComboBox.getItems().add(disposition);
        }

        cachedAutoForwarding = new AutoForwarding();
        logger = Logger.getLogger(getClass().getName());
    }

    public void setForwardingAddresses(List<ForwardingAddress> forwardingAddressList) {
        emailAddressComboBox.getItems().clear();
        emailAddressComboBox.setDisable(true);
        deleteButton.setDisable(true);

        if (forwardingAddressList != null) {
            for (ForwardingAddress forwardingAddress : forwardingAddressList) {
                String forwardingEmail = forwardingAddress.getForwardingEmail();
                String verificationStatus = forwardingAddress.getVerificationStatus();

                if (verificationStatus.equalsIgnoreCase("accepted")) {
                    emailAddressComboBox.getItems().add(forwardingEmail);
                }
            }
        }

        refreshUi(false);
    }

    public void setAutoForwarding(GmailSettingsService service) {
        if (service != null) {
            try {
                List<ForwardingAddress> forwardingAddressList = service.listForwardingAddresses();
                cachedAutoForwarding = service.getAutoForwarding();

                setForwardingAddresses(forwardingAddressList);
                setComponents(cachedAutoForwarding);
                cachedService = service;
            } catch (IOException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        } else {
            cachedAutoForwarding = new AutoForwarding();
            clearComponents();
        }

        refreshUi(false);
    }

    public void enableCheckboxActionPerformed(ActionEvent actionEvent) {
        refreshUi(hasChanges());
    }

    private void clearComponents() {
        enabledCheckBox.setSelected(false);
        emailAddressComboBox.getSelectionModel().clearSelection();
        dispositionComboBox.getSelectionModel().clearSelection();
    }

    private void setComponents(AutoForwarding autoForwarding) {
        enabledCheckBox.setSelected(autoForwarding.getEnabled());
        emailAddressComboBox.getSelectionModel().select(autoForwarding.getEmailAddress());
        dispositionComboBox.getSelectionModel().select(autoForwarding.getDisposition());
    }

    private void refreshUi(boolean hasChanges) {
        if (emailAddressComboBox.getItems().isEmpty()) {
            enabledCheckBox.setDisable(true);
            emailAddressComboBox.setDisable(true);
            deleteButton.setDisable(true);
            dispositionComboBox.setDisable(true);
            resetButton.setDisable(true);
        } else if (enabledCheckBox.isSelected()) {
            enabledCheckBox.setDisable(false);
            emailAddressComboBox.setDisable(false);
            if (!emailAddressComboBox.getItems().isEmpty()) {
                deleteButton.setDisable(false);
            }
            dispositionComboBox.setDisable(false);
            resetButton.setDisable(false);
        } else {
            enabledCheckBox.setDisable(false);
            emailAddressComboBox.setDisable(true);
            deleteButton.setDisable(true);
            dispositionComboBox.setDisable(true);
            resetButton.setDisable(false);
        }

        saveButton.setDisable(!hasChanges);
    }

    private boolean hasChanges() {
        String selectedEmailAddress = emailAddressComboBox.getSelectionModel().getSelectedItem();
        String selectedDisposition = dispositionComboBox.getSelectionModel().getSelectedItem();

        return !cachedAutoForwarding.isEmpty() &&
                (cachedAutoForwarding.getEnabled().compareTo(enabledCheckBox.isSelected()) != 0 ||
                        !cachedAutoForwarding.getEmailAddress().equalsIgnoreCase(selectedEmailAddress) ||
                        !cachedAutoForwarding.getDisposition().equalsIgnoreCase(selectedDisposition));
    }

    public void dispositionComboBoxAction(ActionEvent actionEvent) {
        refreshUi(hasChanges());
    }

    public void emailAddressComboBoxAction(ActionEvent actionEvent) {
        refreshUi(hasChanges());
    }

    public void resetButtonAction(ActionEvent actionEvent) {
        setComponents(cachedAutoForwarding);
        refreshUi(false);
    }

    public void saveButtonAction(ActionEvent actionEvent) {
        AutoForwarding autoForwarding = new AutoForwarding();
        if (enabledCheckBox.isSelected()) {
            autoForwarding.setEnabled(true);
            autoForwarding.setEmailAddress(emailAddressComboBox.getSelectionModel().getSelectedItem());
            autoForwarding.setDisposition(dispositionComboBox.getSelectionModel().getSelectedItem());
        } else {
            autoForwarding.setEnabled(false);
        }

        try {
            cachedAutoForwarding = cachedService.updateAutoForwarding(autoForwarding);
            setComponents(cachedAutoForwarding);
            refreshUi(false);
        } catch (IOException e) {
            Alert exceptionAlert = new ExceptionAlert(e);
            logger.log(Level.SEVERE, e.getMessage(), e);
            exceptionAlert.showAndWait();
        }
    }

    public void createButtonAction(ActionEvent actionEvent) {
        Dialog<String> dialog = new TextInputDialog();
        dialog.setTitle("Forwarding Address");
        dialog.setHeaderText("Enter a new forwarding email.");

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            String forwardingEmail = result.get();

            if (forwardingEmail.isEmpty()) {
                return;
            }

            if(Constants.EMAIL_PATTERN.matcher(forwardingEmail).matches()) {
                ForwardingAddress forwardingAddress = new ForwardingAddress();
                forwardingAddress.setForwardingEmail(forwardingEmail);

                try {
                    cachedService.createForwardingAddress(forwardingAddress);
                } catch (IOException e) {
                    Alert exceptionAlert = new ExceptionAlert(e);
                    logger.log(Level.SEVERE, e.getMessage(), e);
                    exceptionAlert.showAndWait();
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Forwarding address error");
                alert.setHeaderText("Invalid input");
                alert.setContentText(String.format("Forwarding address entered was not valid:\n\n%s", forwardingEmail));
                alert.showAndWait();
            }
        }
    }

    public void deleteButtonAction(ActionEvent actionEvent) {
        String selectedForwardingAddress = emailAddressComboBox.getSelectionModel().getSelectedItem();

        try {
            cachedService.deleteForwardingAddress(selectedForwardingAddress);
            emailAddressComboBox.getItems().remove(selectedForwardingAddress);
            if (emailAddressComboBox.getItems().isEmpty()) {
                AutoForwarding autoForwarding = new AutoForwarding();
                autoForwarding.setEnabled(false);

                cachedService.updateAutoForwarding(autoForwarding);
                cachedAutoForwarding = autoForwarding;
                setComponents(cachedAutoForwarding);
                refreshUi(false);
            }
        } catch (IOException e) {
            Alert exceptionAlert = new ExceptionAlert(e);
            logger.log(Level.SEVERE, e.getMessage(), e);
            exceptionAlert.showAndWait();
        }
    }
}
