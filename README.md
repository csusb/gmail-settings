# README #

## What is this repository for? ##

Quick Summary: gmailSettings is a suite of applications for checking and updating the Gmail 
settings of users in your domain using the Gmail API, Version: 1.23.0

## How do I get set up? ##

Summary of set up: Clone this repo and run the app from the base directory base on platform
 gmailsettings - Unix type OS
 gmailsettings.bat - Windows

 Configuration: Use -h or -help for help with command line options. 
 Dependencies: OpenJDK 11
 
Before using this project in your domain, you must create a project in the Google API console, 
enable the Gmail API, and create a service account.  When done you should note the service 
account ID, client ID, and download the key (p12) file.

Before running the apllications in this suite, you must authorize the Gmail settings scopes below
and run the GmailSettingsConfigure application.

### Scopes ###

In order to use this application you need to authorize the following scopes for your project's client ID.

* https://www.googleapis.com/auth/gmail.settings.basic
* https://www.googleapis.com/auth/gmail.settings.sharing

### GmailSettingsConfigure application ###

Before running any of the command-line applications or the GmailSettingsFX GUI, you must run the 
GmailSettingsConfigure application.  You must collect the following information.

* Domain Name - The domain name of your Gsuite tennant.
* Service Account ID - The account id of the service account you created.
* Service Account Private Key - The key file you downloaded when you created the service account.

## Contribution guidelines ##

* Writing tests
* Code review
* Other guidelines

## Who do I talk to? ##

Repo owner or admin: Chad Cordero/ECS
Other community or team contact: Benjamin K. Derry/ECS

