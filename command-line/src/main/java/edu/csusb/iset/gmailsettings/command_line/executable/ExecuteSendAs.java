package edu.csusb.iset.gmailsettings.command_line.executable;

import com.google.api.services.gmail.model.SendAs;
import edu.csusb.iset.gmailsettings.command_line.cli.SendAsCli;
import edu.csusb.iset.gmailsettings.core.ConnectionNotFoundException;
import edu.csusb.iset.gmailsettings.core.Constants;
import edu.csusb.iset.gmailsettings.core.GmailSettingsService;
import org.apache.commons.cli.ParseException;
import org.dom4j.DocumentException;

import java.io.IOException;
import java.util.List;

public class ExecuteSendAs {

    /**
     * @param args command-line arguments
     * @throws DocumentException           if error occurs parsing connections file
     * @throws IOException                 if send-as email could not be deleted, or if SendAs could not be retrieved, listed or updated
     * @throws ConnectionNotFoundException if no connection is found
     */
    public static void run(String[] args) throws DocumentException, IOException, ConnectionNotFoundException {
        try {
            SendAsCli sendAsCli = new SendAsCli(args);
            String user = sendAsCli.getUser();
            String action = sendAsCli.getAction();
            GmailSettingsService service = new GmailSettingsService(user);

            System.out.printf("User: %s\n", user);
            switch (action) {
                case Constants.ACTION_CREATE -> {
                    SendAs sendAs = sendAsCli.getSendAs();

                    service.createSendAs(sendAs);
                }
                case Constants.ACTION_DELETE -> {
                    String sendAsEmail = sendAsCli.getSendAsEmail();
                    service.deleteSendAs(sendAsEmail);
                }
                case Constants.ACTION_LIST -> {
                    List<SendAs> sendAsList = service.listSendAs();
                    int i = 1;

                    for (SendAs sendAs : sendAsList) {
                        System.out.printf("SendAs %d:\n" +
                                        "\tsendAsEmail: %s\n" +
                                        "\tdisplayName: %s\n" +
                                        "\treplyToAddress: %s\n" +
                                        "\tsignature: %s\n" +
                                        "\tisPrimary: %s\n" +
                                        "\tisDefault: %s\n" +
                                        "\ttreatAsAlias: %s\n" +
                                        "\tverificationStatus: %s\n",
                                i,
                                sendAs.getSendAsEmail(),
                                sendAs.getDisplayName(),
                                sendAs.getReplyToAddress(),
                                sendAs.getSignature(),
                                sendAs.getIsPrimary(),
                                sendAs.getIsDefault(),
                                sendAs.getTreatAsAlias(),
                                sendAs.getVerificationStatus());
                        i++;
                    }
                }
                case Constants.ACTION_UPDATE -> {
                    SendAs sendAs = sendAsCli.getSendAs();

                    service.updateSendAs(sendAs);
                }
            }
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            SendAsCli.printHelp();
        }
    }
}
