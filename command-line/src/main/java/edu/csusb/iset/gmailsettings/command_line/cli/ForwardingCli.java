package edu.csusb.iset.gmailsettings.command_line.cli;

import com.google.api.services.gmail.model.ForwardingAddress;
import edu.csusb.iset.gmailsettings.core.Constants;
import org.apache.commons.cli.*;

import java.util.Collections;
import java.util.List;

public class ForwardingCli {

    public static final String HELP_CMD = "forwarding";
    public static final String HELP_HEADER = "\nManages forwarding addresses for a user.\n\n";

    private static final Option createOption = Option.builder(Constants.ACTION_CREATE)
            .desc("Create a forwarding address")
            .build();
    private static final Option deleteOption = Option.builder(Constants.ACTION_DELETE)
            .desc("Delete a forwarding address")
            .build();
    private static final Option listOption = Option.builder(Constants.ACTION_LIST)
            .desc("List all forwarding addresses")
            .build();
    private static final Option userOption = Option.builder(Constants.OPTION_USER)
            .hasArg()
            .argName("email")
            .desc("Specifies the email address of the user.")
            .required()
            .build();
    private static final Option forwardingEmailOPTION = Option.builder(Constants.OPTION_FORWARDING_EMAIL)
            .hasArg()
            .argName("email")
            .desc("An email address to which messages can be forwarded.")
            .build();

    private static final OptionGroup actionGroup = new OptionGroup();
    private static final Options options = new Options();

    static {
        actionGroup.addOption(createOption);
        actionGroup.addOption(deleteOption);
        actionGroup.addOption(listOption);
        actionGroup.setRequired(true);

        options.addOptionGroup(actionGroup);
        options.addOption(userOption);
        options.addOption(forwardingEmailOPTION);
    }

    private final CommandLine commandLine;

    public ForwardingCli(String[] args) throws ParseException {
        DefaultParser parser = new DefaultParser();
        commandLine = parser.parse(options, args);
    }

    public static void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.setOptionComparator(null);

        formatter.printHelp(String.format("%s %s", Constants.APPLICATION_NAME, HELP_CMD), HELP_HEADER, options, Constants.HELP_FOOTER, true);
    }

    public String getAction() {
        return actionGroup.getSelected();
    }

    public String getUser() throws ParseException {
        String user = commandLine.getOptionValue(Constants.OPTION_USER);

        if (!Constants.EMAIL_PATTERN.matcher(user).matches()) {
            throw new IllegalArgumentException(userOption);
        }

        return user;
    }

    public String getForwardingEmail() throws ParseException {
        String forwardingEmail;

        if (commandLine.hasOption(Constants.OPTION_FORWARDING_EMAIL)) {
            forwardingEmail = commandLine.getOptionValue(Constants.OPTION_FORWARDING_EMAIL);

            if (!Constants.EMAIL_PATTERN.matcher(forwardingEmail).matches()) {
                throw new IllegalArgumentException(forwardingEmailOPTION);
            }
        } else {
            List<String> missingOptions = Collections.singletonList(Constants.OPTION_FORWARDING_EMAIL);

            throw new MissingOptionException(missingOptions);
        }

        return forwardingEmail;
    }

    public ForwardingAddress getForwardingAddress() throws ParseException {
        ForwardingAddress forwardingAddress = new ForwardingAddress();

        if (commandLine.hasOption(Constants.OPTION_FORWARDING_EMAIL)) {
            String forwardingEmail = commandLine.getOptionValue(Constants.OPTION_FORWARDING_EMAIL);

            if (Constants.EMAIL_PATTERN.matcher(forwardingEmail).matches()) {
                forwardingAddress.setForwardingEmail(forwardingEmail);
            } else {
                throw new IllegalArgumentException(forwardingEmailOPTION);
            }
        } else {
            List<String> missingOptions = Collections.singletonList(Constants.OPTION_FORWARDING_EMAIL);

            throw new MissingOptionException(missingOptions);
        }

        return forwardingAddress;
    }
}
