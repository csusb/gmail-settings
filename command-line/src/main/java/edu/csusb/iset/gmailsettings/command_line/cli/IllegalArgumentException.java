package edu.csusb.iset.gmailsettings.command_line.cli;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;

@SuppressWarnings("unused")
public class IllegalArgumentException extends ParseException {
    private Option option;

    public IllegalArgumentException(final String message) {
        super(message);
    }

    public IllegalArgumentException(final Option option) {
        this(String.format("Illegal argument for option: %s", option.getOpt()));
        this.option = option;
    }

    public Option getOption() {
        return option;
    }
}
