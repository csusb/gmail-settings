package edu.csusb.iset.gmailsettings.command_line.cli;

import com.google.api.services.gmail.model.PopSettings;
import edu.csusb.iset.gmailsettings.core.Constants;
import org.apache.commons.cli.*;

import java.util.Collections;
import java.util.List;

public class PopCli {

    public static final String HELP_CMD = "pop";
    public static final String HELP_HEADER = "\nManages POP settings for a user.\n\n";

    private static final Option getOption = Option.builder(Constants.ACTION_GET)
            .desc("Get POP settings")
            .build();
    private static final Option updateOption = Option.builder(Constants.ACTION_UPDATE)
            .desc("Update POP settings")
            .build();
    private static final Option userOption = Option.builder(Constants.OPTION_USER)
            .hasArg()
            .argName("email")
            .desc("Specifies the email address of the user.")
            .required()
            .build();
    private static final Option accessWindowOption = Option.builder(Constants.OPTION_ACCESS_WINDOW)
            .hasArg()
            .argName("range")
            .desc("The range of messages which are accessible via POP. Not used with get.\n\n"
                    + "Acceptable values are:\n\n"
                    + "\tallMail: Indicates that all un-fetched messages are accessible via POP.\n"
                    + "\tdisabled: Indicates that no messages are accessible via POP.\n"
                    + "\tfromNowOn: Indicates that un-fetched messages received "
                    + "after some past point in time are accessible via POP.")
            .build();
    private static final Option dispositionOption = Option.builder(Constants.OPTION_DISPOSITION)
            .hasArg()
            .argName("action")
            .desc("The action that will be executed on a message after it has been fetched via POP. Not used with get.\n\n"
                    + "Acceptable values are:\n\n"
                    + "\tarchive: Archive the message.\n"
                    + "\tleaveInInbox: Leave the message in the INBOX.\n"
                    + "\tmarkRead: Leave the message in the INBOX and mark it as read.\n"
                    + "\ttrash: Move the message to the TRASH.")
            .build();

    private static final OptionGroup actionGroup = new OptionGroup();
    private static final Options options = new Options();

    static {
        actionGroup.addOption(getOption);
        actionGroup.addOption(updateOption);
        actionGroup.setRequired(true);

        options.addOptionGroup(actionGroup);
        options.addOption(userOption);
        options.addOption(accessWindowOption);
        options.addOption(dispositionOption);
    }

    private final CommandLine commandLine;

    public PopCli(String[] args) throws ParseException {
        DefaultParser parser = new DefaultParser();
        commandLine = parser.parse(options, args);
    }

    public static void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.setOptionComparator(null);

        formatter.printHelp(String.format("%s %s", Constants.APPLICATION_NAME, HELP_CMD), HELP_HEADER, options, Constants.HELP_FOOTER, true);
    }

    public String getAction() {
        return actionGroup.getSelected();
    }

    public String getUser() throws ParseException {
        String user = commandLine.getOptionValue(Constants.OPTION_USER);

        if (!Constants.EMAIL_PATTERN.matcher(user).matches()) {
            throw new IllegalArgumentException(userOption);
        }

        return user;
    }

    public PopSettings getPopSettings() throws ParseException {
        PopSettings popSettings = new PopSettings();

        if (commandLine.hasOption(Constants.OPTION_ACCESS_WINDOW)) {
            String accessWindow = commandLine.getOptionValue(Constants.OPTION_ACCESS_WINDOW);

            if (accessWindow.equalsIgnoreCase("DISABLED")) {
                popSettings.setAccessWindow(accessWindow);
            } else if (accessWindow.equalsIgnoreCase("FROM_NOW_ON") || accessWindow.equalsIgnoreCase("ALL_MAIL")) {
                if (commandLine.hasOption(Constants.OPTION_DISPOSITION)) {
                    String disposition = commandLine.getOptionValue(Constants.OPTION_DISPOSITION);

                    if (Constants.POP_DISPOSITION.contains(disposition)) {
                        popSettings.setAccessWindow(accessWindow);
                        popSettings.setDisposition(disposition);
                    } else {
                        throw new IllegalArgumentException(dispositionOption);
                    }
                } else {
                    List<String> missingOptions = Collections.singletonList(Constants.OPTION_DISPOSITION);

                    throw new MissingOptionException(missingOptions);
                }
            } else {
                throw new IllegalArgumentException(accessWindowOption);
            }
        } else {
            List<String> missingOptions = Collections.singletonList(Constants.OPTION_ACCESS_WINDOW);

            throw new MissingOptionException(missingOptions);
        }

        return popSettings;
    }
}
