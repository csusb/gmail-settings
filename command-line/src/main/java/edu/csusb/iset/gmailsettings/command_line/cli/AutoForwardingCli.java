package edu.csusb.iset.gmailsettings.command_line.cli;

import com.google.api.services.gmail.model.AutoForwarding;
import edu.csusb.iset.gmailsettings.core.Constants;
import org.apache.commons.cli.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AutoForwardingCli {

    public static final String HELP_CMD = "autoForwarding";
    public static final String HELP_HEADER = "\nManages auto-forwarding settings for a user.\n\n";

    private static final Option getOption = Option.builder(Constants.ACTION_GET)
            .desc("Get the auto-forwarding address")
            .build();
    private static final Option updateOption = Option.builder(Constants.ACTION_UPDATE)
            .desc("Update the auto-forwarding address")
            .build();
    private static final Option userOption = Option.builder(Constants.OPTION_USER)
            .hasArg()
            .argName("email")
            .desc("Specifies the email address of the user.")
            .required()
            .build();
    private static final Option enabledOption = Option.builder(Constants.OPTION_ENABLED)
            .hasArg()
            .argName("boolean")
            .desc("Whether all incoming mail is automatically forwarded to another address. Not used with get.")
            .build();
    private static final Option emailAddressOption = Option.builder(Constants.OPTION_EMAIL_ADDRESS)
            .hasArg()
            .argName("email")
            .desc("Email address to which all incoming messages are forwarded. This email address must be a "
                    + "verified member of the forwarding addresses. Not used with get.")
            .build();
    private static final Option dispositionOption = Option.builder(Constants.OPTION_DISPOSITION)
            .hasArg()
            .argName("state")
            .desc("The state that a message should be left in after it has been forwarded. Not used with get.\n\n"
                    + "Acceptable values are:\n\n"
                    + "\tarchive: Archive the message.\n"
                    + "\tleaveInInbox: Leave the message in the INBOX.\n"
                    + "\tmarkRead: Leave the message in the INBOX and mark it as read.\n"
                    + "\ttrash: Move the message to the TRASH.")
            .build();

    private static final OptionGroup actionGroup = new OptionGroup();
    private static final Options options = new Options();

    static {
        actionGroup.addOption(getOption);
        actionGroup.addOption(updateOption);
        actionGroup.setRequired(true);

        options.addOptionGroup(actionGroup);
        options.addOption(userOption);
        options.addOption(enabledOption);
        options.addOption(emailAddressOption);
        options.addOption(dispositionOption);
    }

    private final CommandLine commandLine;

    public AutoForwardingCli(String[] args) throws ParseException {
        DefaultParser parser = new DefaultParser();
        commandLine = parser.parse(options, args);
    }

    public static void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.setOptionComparator(null);

        formatter.printHelp(String.format("%s %s", Constants.APPLICATION_NAME, HELP_CMD), HELP_HEADER, options, Constants.HELP_FOOTER, true);
    }

    public String getAction() {
        return actionGroup.getSelected();
    }

    public String getUser() throws ParseException {
        String user;

        if (commandLine.hasOption(Constants.OPTION_USER)) {
            user = commandLine.getOptionValue(Constants.OPTION_USER);

            if (!Constants.EMAIL_PATTERN.matcher(user).matches()) {
                throw new IllegalArgumentException(userOption);
            }
        } else {
            List<String> missingOptions = Collections.singletonList(Constants.OPTION_USER);

            throw new MissingOptionException(missingOptions);
        }

        return user;
    }

    public AutoForwarding getAutoForwarding() throws ParseException {
        AutoForwarding autoForwarding = new AutoForwarding();

        if (commandLine.hasOption(Constants.OPTION_ENABLED)) {
            String enabled = commandLine.getOptionValue(Constants.OPTION_ENABLED);

            if (enabled.equalsIgnoreCase("false")) {
                autoForwarding.setEnabled(false);
            } else if (enabled.equalsIgnoreCase("true")) {
                autoForwarding.setEnabled(true);

                if (commandLine.hasOption(Constants.OPTION_EMAIL_ADDRESS) && commandLine.hasOption(Constants.OPTION_DISPOSITION)) {
                    String emailAddress = commandLine.getOptionValue(Constants.OPTION_EMAIL_ADDRESS);
                    String disposition = commandLine.getOptionValue(Constants.OPTION_DISPOSITION);

                    if (Constants.EMAIL_PATTERN.matcher(emailAddress).matches()) {
                        autoForwarding.setEmailAddress(emailAddress);
                    } else {
                        throw new IllegalArgumentException(emailAddressOption);
                    }

                    if (Constants.AUTO_FORWARDING_DISPOSITION.contains(disposition)) {
                        autoForwarding.setDisposition(disposition);
                    } else {
                        throw new IllegalArgumentException(dispositionOption);
                    }
                } else {
                    List<String> missingOptions = new ArrayList<>();
                    missingOptions.add(Constants.OPTION_EMAIL_ADDRESS);
                    missingOptions.add(Constants.OPTION_DISPOSITION);

                    throw new MissingOptionException(missingOptions);
                }
            }
        } else {
            List<String> missingOptions = Collections.singletonList(Constants.OPTION_ENABLED);

            throw new MissingOptionException(missingOptions);
        }

        return autoForwarding;
    }
}
