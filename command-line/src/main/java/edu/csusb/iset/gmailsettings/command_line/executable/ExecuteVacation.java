package edu.csusb.iset.gmailsettings.command_line.executable;

import com.google.api.services.gmail.model.VacationSettings;
import edu.csusb.iset.gmailsettings.command_line.cli.VacationCli;
import edu.csusb.iset.gmailsettings.core.ConnectionNotFoundException;
import edu.csusb.iset.gmailsettings.core.Constants;
import edu.csusb.iset.gmailsettings.core.GmailSettingsService;
import org.apache.commons.cli.ParseException;
import org.dom4j.DocumentException;

import java.io.IOException;
import java.util.Date;

public class ExecuteVacation {

    /**
     * @param args command-line arguments
     * @throws DocumentException           if error occurs parsing connections file
     * @throws IOException                 if VacationSettings could not be retrieved or updated
     * @throws ConnectionNotFoundException if no connection is found
     */
    public static void run(String[] args) throws DocumentException, IOException, ConnectionNotFoundException {
        try {
            VacationCli vacationCli = new VacationCli(args);
            String user = vacationCli.getUser();
            String action = vacationCli.getAction();
            GmailSettingsService service = new GmailSettingsService(user);

            System.out.printf("User: %s\n", user);
            if (action.equals(Constants.ACTION_GET)) {
                VacationSettings vacationSettings = service.getVacationSettings();

                System.out.printf("Vacation:\n" +
                                "\tenableAutoReply: %s\n" +
                                "\tresponseSubject: %s\n" +
                                "\tresponseBodyPlainText: %s\n" +
                                "\tresponseBodyHtml: %s\n" +
                                "\trestrictToContacts: %s\n" +
                                "\trestrictToDomain: %s\n" +
                                "\tstartTime: %s\n" +
                                "\tendTime: %s\n",
                        vacationSettings.getEnableAutoReply(),
                        vacationSettings.getResponseSubject(),
                        vacationSettings.getResponseBodyPlainText(),
                        vacationSettings.getResponseBodyHtml(),
                        vacationSettings.getRestrictToContacts(),
                        vacationSettings.getRestrictToDomain(),
                        vacationSettings.getStartTime() != null ? new Date(vacationSettings.getStartTime()).toString() : null,
                        vacationSettings.getEndTime() != null ? new Date(vacationSettings.getEndTime()).toString() : null);
            } else if (action.equals(Constants.ACTION_UPDATE)) {
                VacationSettings vacationSettings = vacationCli.getVacationSettings();

                service.updateVacationSettings(vacationSettings);
            }
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            VacationCli.printHelp();
        }
    }
}
