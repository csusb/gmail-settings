package edu.csusb.iset.gmailsettings.command_line.executable;

import com.google.api.services.gmail.model.ForwardingAddress;
import edu.csusb.iset.gmailsettings.command_line.cli.ForwardingCli;
import edu.csusb.iset.gmailsettings.core.ConnectionNotFoundException;
import edu.csusb.iset.gmailsettings.core.Constants;
import edu.csusb.iset.gmailsettings.core.GmailSettingsService;
import org.apache.commons.cli.ParseException;
import org.dom4j.DocumentException;

import java.io.IOException;
import java.util.List;

public class ExecuteForwarding {

    /**
     * @param args command-line arguments
     * @throws DocumentException           if error occurs parsing connections file
     * @throws IOException                 if forwarding email could not be deleted or, if ForwardingAddress could not be created or listed
     * @throws ConnectionNotFoundException if no connection is found
     */
    public static void run(String[] args) throws DocumentException, IOException, ConnectionNotFoundException {
        try {
            ForwardingCli forwardingCli = new ForwardingCli(args);
            String user = forwardingCli.getUser();
            String action = forwardingCli.getAction();
            GmailSettingsService service = new GmailSettingsService(user);

            switch (action) {
                case Constants.ACTION_CREATE -> {
                    ForwardingAddress forwardingAddress = forwardingCli.getForwardingAddress();
                    service.createForwardingAddress(forwardingAddress);
                }
                case Constants.ACTION_DELETE -> {
                    String forwardingEmail = forwardingCli.getForwardingEmail();
                    service.deleteForwardingAddress(forwardingEmail);
                }
                case Constants.ACTION_LIST -> {
                    List<ForwardingAddress> forwardingAddressList = service.listForwardingAddresses();
                    System.out.printf("User: %s\n", user);
                    if (forwardingAddressList != null) {
                        int i = 1;

                        for (ForwardingAddress forwardingAddress : forwardingAddressList) {
                            System.out.printf("Forwarding %d:\n" +
                                            "\tforwardingEmail: %s\n" +
                                            "\tverificationStatus: %s\n",
                                    i,
                                    forwardingAddress.getForwardingEmail(),
                                    forwardingAddress.getVerificationStatus());
                            i++;
                        }
                    } else {
                        System.out.println("No forwarding addresses found.");
                    }
                }
            }
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            ForwardingCli.printHelp();
        }
    }
}
