package edu.csusb.iset.gmailsettings.command_line.cli;

import com.google.api.services.gmail.model.ImapSettings;
import edu.csusb.iset.gmailsettings.core.Constants;
import org.apache.commons.cli.*;

import java.util.Collections;
import java.util.List;

public class ImapCli {

    public static final String HELP_CMD = "imap";
    public static final String HELP_HEADER = "\nManages IMAP settings for a user.\n\n";

    private static final Option getOption = Option.builder(Constants.ACTION_GET)
            .desc("Get IMAP settings")
            .build();
    private static final Option updateOption = Option.builder(Constants.ACTION_UPDATE)
            .desc("Update IMAP settings")
            .build();
    private static final Option userOption = Option.builder(Constants.OPTION_USER)
            .hasArg()
            .argName("email")
            .desc("Specifies the email address of the user.")
            .required()
            .build();
    private static final Option enabledOption = Option.builder(Constants.OPTION_ENABLED)
            .hasArg()
            .argName("boolean")
            .desc("Whether IMAP is enabled for the account. Not used with get.")
            .build();
    private static final Option autoExpungeOption = Option.builder(Constants.OPTION_AUTO_EXPUNGE)
            .hasArg()
            .argName("boolean")
            .desc("If this value is true, Gmail will immediately expunge a message "
                    + "when it is marked as deleted in IMAP. Otherwise, Gmail will wait "
                    + "for an update from the client before expunging messages marked "
                    + "as deleted. Not used with get.")
            .build();
    private static final Option expungeBehaviorOption = Option.builder(Constants.OPTION_EXPUNGE_BEHAVIOR)
            .hasArg()
            .argName("action")
            .desc("The action that will be executed on a message when it is marked "
                    + "as deleted and expunged from the last visible IMAP folder. Not used with get.\n\n"
                    + "Acceptable values are:\n\n"
                    + "\tarchive: Archive messages marked as deleted.\n"
                    + "\tdeleteForever: Immediately and permanently delete messages marked as deleted. The expunged messages cannot be recovered.\n"
                    + "\ttrash: Move messages marked as deleted to the trash.")
            .build();
    private static final Option maxFolderSizeOption = Option.builder(Constants.OPTION_MAX_FOLDER_SIZE)
            .hasArg()
            .argName("limit")
            .desc("An optional limit on the number of messages that an IMAP folder "
                    + "may contain. Legal values are 0, 1000, 2000, 5000 or 10000. A "
                    + "value of zero is interpreted to mean that there is no limit. Not used with get.")
            .build();

    private static final OptionGroup actionGroup = new OptionGroup();
    private static final Options options = new Options();

    static {
        actionGroup.addOption(getOption);
        actionGroup.addOption(updateOption);
        actionGroup.setRequired(true);

        options.addOptionGroup(actionGroup);
        options.addOption(userOption);
        options.addOption(enabledOption);
        options.addOption(autoExpungeOption);
        options.addOption(expungeBehaviorOption);
        options.addOption(maxFolderSizeOption);
    }

    private final CommandLine commandLine;

    public ImapCli(String[] args) throws ParseException {
        DefaultParser parser = new DefaultParser();
        commandLine = parser.parse(options, args);
    }

    public static void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.setOptionComparator(null);

        formatter.printHelp(String.format("%s %s", Constants.APPLICATION_NAME, HELP_CMD), HELP_HEADER, options, Constants.HELP_FOOTER, true);
    }

    public String getAction() {
        return actionGroup.getSelected();
    }

    public String getUser() throws ParseException {
        String user = commandLine.getOptionValue(Constants.OPTION_USER);

        if (!Constants.EMAIL_PATTERN.matcher(user).matches()) {
            throw new IllegalArgumentException(userOption);
        }

        return user;
    }

    public ImapSettings getImapSettings() throws ParseException {
        ImapSettings imapSettings = new ImapSettings();

        if (commandLine.hasOption(Constants.OPTION_ENABLED)) {
            String enabled = commandLine.getOptionValue(Constants.OPTION_ENABLED);

            if (enabled.equalsIgnoreCase("false")) {
                imapSettings.setEnabled(false);
            } else if (enabled.equalsIgnoreCase("true")) {
                imapSettings.setEnabled(true);

                if (commandLine.hasOption(Constants.OPTION_AUTO_EXPUNGE)) {
                    String autoExpunge = commandLine.getOptionValue(Constants.OPTION_AUTO_EXPUNGE);

                    if (autoExpunge.equalsIgnoreCase("true")) {
                        imapSettings.setAutoExpunge(true);
                    } else if (autoExpunge.equalsIgnoreCase("false")) {
                        imapSettings.setAutoExpunge(false);

                        if (commandLine.hasOption(Constants.OPTION_EXPUNGE_BEHAVIOR)) {
                            String expungeBehavior = commandLine.getOptionValue(Constants.OPTION_EXPUNGE_BEHAVIOR);

                            if (Constants.IMAP_EXPUNGE_BEHAVIOR.contains(expungeBehavior)) {
                                imapSettings.setExpungeBehavior(expungeBehavior);
                            } else {
                                throw new IllegalArgumentException(Constants.OPTION_EXPUNGE_BEHAVIOR);
                            }
                        } else {
                            List<String> missingOptions = Collections.singletonList(Constants.OPTION_EXPUNGE_BEHAVIOR);

                            throw new MissingOptionException(missingOptions);
                        }

                        if (commandLine.hasOption(Constants.OPTION_MAX_FOLDER_SIZE)) {
                            Integer maxFolderSize = Integer.parseInt(commandLine.getOptionValue(Constants.OPTION_MAX_FOLDER_SIZE));

                            if (Constants.IMAP_MAX_FOLDER_SIZE.contains(maxFolderSize)) {
                                imapSettings.setMaxFolderSize(maxFolderSize);
                            } else {
                                throw new IllegalArgumentException(maxFolderSizeOption);
                            }
                        } else {
                            List<String> missingOptions = Collections.singletonList(Constants.OPTION_MAX_FOLDER_SIZE);

                            throw new MissingOptionException(missingOptions);
                        }
                    }
                }
            } else {
                throw new IllegalArgumentException(enabledOption);
            }
        } else {
            List<String> missingOptions = Collections.singletonList(Constants.OPTION_ENABLED);

            throw new MissingOptionException(missingOptions);
        }

        return imapSettings;
    }
}
