package edu.csusb.iset.gmailsettings.command_line.executable;

import com.google.api.services.gmail.model.ImapSettings;
import edu.csusb.iset.gmailsettings.command_line.cli.ImapCli;
import edu.csusb.iset.gmailsettings.core.ConnectionNotFoundException;
import edu.csusb.iset.gmailsettings.core.Constants;
import edu.csusb.iset.gmailsettings.core.GmailSettingsService;
import org.apache.commons.cli.ParseException;
import org.dom4j.DocumentException;

import java.io.IOException;

public class ExecuteImap {

    /**
     * @param args command-line arguments
     * @throws DocumentException           if error occurs parsing connections file
     * @throws IOException                 if ImapSettings could not be retrieved or updated
     * @throws ConnectionNotFoundException if not connection is found
     */
    public static void run(String[] args) throws DocumentException, IOException, ConnectionNotFoundException {
        try {
            ImapCli imapCli = new ImapCli(args);
            String user = imapCli.getUser();
            String action = imapCli.getAction();
            GmailSettingsService service = new GmailSettingsService(user);

            System.out.printf("User: %s\n", user);
            if (action.equals(Constants.ACTION_GET)) {
                ImapSettings imapSettings = service.getImapSettings();

                System.out.printf("IMAP:\n" +
                                "\tenabled: %s\n" +
                                "\tautoExpunge: %s\n" +
                                "\texpungeBehavior: %s\n" +
                                "\tmaxFolderSize: %s\n",
                        imapSettings.getEnabled(),
                        imapSettings.getAutoExpunge(),
                        imapSettings.getExpungeBehavior(),
                        imapSettings.getMaxFolderSize());
            } else if (action.equals(Constants.ACTION_UPDATE)) {
                ImapSettings imapSettings = imapCli.getImapSettings();

                service.updateImapSettings(imapSettings);
            }
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            ImapCli.printHelp();
        }
    }
}
