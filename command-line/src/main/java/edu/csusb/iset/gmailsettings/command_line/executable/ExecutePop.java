package edu.csusb.iset.gmailsettings.command_line.executable;

import com.google.api.services.gmail.model.PopSettings;
import edu.csusb.iset.gmailsettings.command_line.cli.PopCli;
import edu.csusb.iset.gmailsettings.core.ConnectionNotFoundException;
import edu.csusb.iset.gmailsettings.core.Constants;
import edu.csusb.iset.gmailsettings.core.GmailSettingsService;
import org.apache.commons.cli.ParseException;
import org.dom4j.DocumentException;

import java.io.IOException;

public class ExecutePop {

    /**
     * @param args command-line arguments
     * @throws DocumentException           if error occurs parsing connections file
     * @throws IOException                 if PopSettings could not be retrieved or updated
     * @throws ConnectionNotFoundException if no connection is found
     */
    public static void run(String[] args) throws DocumentException, IOException, ConnectionNotFoundException {
        try {
            PopCli popCli = new PopCli(args);
            String user = popCli.getUser();
            String action = popCli.getAction();
            GmailSettingsService service = new GmailSettingsService(user);

            System.out.printf("User: %s\n", user);
            if (action.equals(Constants.ACTION_GET)) {
                PopSettings popSettings = service.getPopSettings();

                System.out.printf("POP:\n" +
                                "\taccessWindow: %s\n" +
                                "\tdisposition: %s\n",
                        popSettings.getAccessWindow(),
                        popSettings.getDisposition());
            } else if (action.equals(Constants.ACTION_UPDATE)) {
                PopSettings popSettings = popCli.getPopSettings();

                service.updatePopSettings(popSettings);
            }
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            PopCli.printHelp();
        }
    }
}
