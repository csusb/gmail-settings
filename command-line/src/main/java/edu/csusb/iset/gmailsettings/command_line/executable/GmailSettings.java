package edu.csusb.iset.gmailsettings.command_line.executable;

import edu.csusb.iset.gmailsettings.command_line.cli.*;
import edu.csusb.iset.gmailsettings.core.ApplicationLogger;
import edu.csusb.iset.gmailsettings.core.ConnectionNotFoundException;
import edu.csusb.iset.gmailsettings.core.Constants;
import org.dom4j.DocumentException;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class GmailSettings {

    public static void main(String[] args) {
        String subCommand = args.length > 0 ? args[0] : "";
        LogManager.getLogManager().addLogger(ApplicationLogger.getLogger(""));

        Logger logger = Logger.getLogger(GmailSettings.class.getName());
        try {
            switch (subCommand) {
                case Constants.COMMAND_AUTO_FORWARDING -> ExecuteAutoForwarding.run(args);
                case Constants.COMMAND_FORWARDING -> ExecuteForwarding.run(args);
                case Constants.COMMAND_IMAP -> ExecuteImap.run(args);
                case Constants.COMMAND_POP -> ExecutePop.run(args);
                case Constants.COMMAND_SEND_AS -> ExecuteSendAs.run(args);
                case Constants.COMMAND_VACATION -> ExecuteVacation.run(args);
                default -> System.out.printf("%s <cmd>\n\n" +
                                "Manages Gmail settings for a user, where <cmd> can be one of the following:\n\n" +
                                "%s - %s\n" +
                                "%s - %s\n" +
                                "%s - %s\n" +
                                "%s - %s\n" +
                                "%s - %s\n" +
                                "%s - %s\n\n" +
                                "%s\n",
                        Constants.APPLICATION_NAME,
                        AutoForwardingCli.HELP_CMD, AutoForwardingCli.HELP_HEADER.strip(),
                        ForwardingCli.HELP_CMD, ForwardingCli.HELP_HEADER.strip(),
                        ImapCli.HELP_CMD, ImapCli.HELP_HEADER.strip(),
                        PopCli.HELP_CMD, PopCli.HELP_HEADER.strip(),
                        SendAsCli.HELP_CMD, SendAsCli.HELP_HEADER.strip(),
                        VacationCli.HELP_CMD, VacationCli.HELP_HEADER.strip(),
                        Constants.HELP_FOOTER.strip());
            }
        } catch (ConnectionNotFoundException e) {
            System.err.println("Connection not found.");
        } catch (IOException | DocumentException e) {
            logger.log(Level.SEVERE, "Exception in GmailSettings cli", e);
        }
    }
}
