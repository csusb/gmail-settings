package edu.csusb.iset.gmailsettings.command_line.executable;

import com.google.api.services.gmail.model.AutoForwarding;
import edu.csusb.iset.gmailsettings.command_line.cli.AutoForwardingCli;
import edu.csusb.iset.gmailsettings.core.ConnectionNotFoundException;
import edu.csusb.iset.gmailsettings.core.Constants;
import edu.csusb.iset.gmailsettings.core.GmailSettingsService;
import org.apache.commons.cli.ParseException;
import org.dom4j.DocumentException;

import java.io.IOException;

public class ExecuteAutoForwarding {

    /**
     * @param args command-line arguments
     * @throws DocumentException           if error occurs parsing connections file
     * @throws IOException                 if AutoForwarding could not be retrieved or updated
     * @throws ConnectionNotFoundException if no connection is found
     */
    public static void run(String[] args) throws DocumentException, IOException, ConnectionNotFoundException {
        try {
            AutoForwardingCli autoForwardingCli = new AutoForwardingCli(args);
            String action = autoForwardingCli.getAction();
            String user = autoForwardingCli.getUser();
            GmailSettingsService service = new GmailSettingsService(user);

            System.out.printf("User: %s\n", user);
            if (action.equals(Constants.ACTION_GET)) {
                AutoForwarding autoForwarding = service.getAutoForwarding();
                System.out.printf("autoForwarding:\n" +
                                "\tenabled: %s\n" +
                                "\temailAddress: %s\n" +
                                "\tdisposition: %s\n",
                        autoForwarding.getEnabled(),
                        autoForwarding.getEmailAddress(),
                        autoForwarding.getDisposition());
            } else if (action.equals(Constants.ACTION_UPDATE)) {
                AutoForwarding autoForwarding = autoForwardingCli.getAutoForwarding();
                service.updateAutoForwarding(autoForwarding);
            }
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            AutoForwardingCli.printHelp();
        }
    }
}
