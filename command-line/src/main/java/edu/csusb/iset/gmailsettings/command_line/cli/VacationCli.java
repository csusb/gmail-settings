package edu.csusb.iset.gmailsettings.command_line.cli;

import com.google.api.services.gmail.model.VacationSettings;
import edu.csusb.iset.gmailsettings.core.Constants;
import edu.csusb.iset.gmailsettings.core.DateString;
import org.apache.commons.cli.*;

import java.util.Collections;
import java.util.List;
import java.util.TimeZone;

public class VacationCli {

    public static final String HELP_CMD = "vacation";
    public static final String HELP_HEADER = "\nManages vacation responder settings for a user.\n\n";

    private static final Option getOption = Option.builder(Constants.ACTION_GET)
            .desc("Get vacation auto-responder")
            .build();
    private static final Option updateOption = Option.builder(Constants.ACTION_UPDATE)
            .desc("Update vacation auto-responder")
            .build();
    private static final Option userOption = Option.builder(Constants.OPTION_USER)
            .hasArg()
            .argName("email")
            .desc("Specifies the email address of the user.")
            .required()
            .build();
    private static final Option enableAutoReplyOption = Option.builder(Constants.OPTION_ENABLE_AUTO_REPLY)
            .hasArg()
            .argName("boolean")
            .desc("Whether Gmail automatically replies to messages. Not used with get.")
            .build();
    private static final Option responseSubjectOption = Option.builder(Constants.OPTION_RESPONSE_SUBJECT)
            .hasArg()
            .argName("subject")
            .desc("Optional text to prepend to the subject line in vacation "
                    + "responses. In order to enable auto-replies, either the "
                    + "response subject or the response body must be "
                    + "nonempty. Not used with get.")
            .build();
    private static final Option responseBodyPlainTextOption = Option.builder(Constants.OPTION_RESPONSE_BODY_PLAIN_TEXT)
            .hasArg()
            .argName("body")
            .desc("Response body in plain text format. Not used with get.")
            .build();
    private static final Option responseBodyHtmlOption = Option.builder(Constants.OPTION_RESPONSE_BODY_HTML)
            .hasArg()
            .argName("body")
            .desc("Response body in HTML format. Gmail will sanitize the "
                    + "HTML before storing it. Not used with get.")
            .build();
    private static final Option restrictToContactsOption = Option.builder(Constants.OPTION_RESTRICT_TO_CONTACTS)
            .hasArg()
            .argName("boolean")
            .desc("Responses are sent to recipients who are not in the user's list of contacts. Not used with get.")
            .build();
    private static final Option restrictToDomainOption = Option.builder(Constants.OPTION_RESTRICT_TO_DOMAIN)
            .hasArg()
            .argName("boolean")
            .desc("Responses are sent to recipients who are outside of the user's domain. This feature is only available for G Suite users. Not used with get.")
            .build();
    private static final Option startTimeOption = Option.builder(Constants.OPTION_START_TIME)
            .hasArg()
            .argName("dateTime")
            .desc("An optional start time for sending auto-replies. "
                    + "When this is specified, Gmail will automatically reply "
                    + "only to messages that it receives after the start time. If "
                    + "both startTime and endTime are specified, startTime "
                    + "must precede endTime. Not used with get.")
            .build();
    private static final Option endTimeOption = Option.builder(Constants.OPTION_END_TIME)
            .hasArg()
            .argName("dateTime")
            .desc("An optional end time for sending auto-replies. "
                    + "When this is specified, Gmail will automatically reply only "
                    + "to messages that it receives before the end time. If both "
                    + "startTime and endTime are specified, startTime "
                    + "must precede endTime. Not used with get.")
            .build();
    private static final TimeZone timeZone = TimeZone.getDefault();

    private static final OptionGroup actionGroup = new OptionGroup();
    private static final Options options = new Options();

    static {
        actionGroup.addOption(getOption);
        actionGroup.addOption(updateOption);
        actionGroup.setRequired(true);

        OptionGroup responseBodyOptionGroup = new OptionGroup();
        responseBodyOptionGroup.addOption(responseBodyPlainTextOption);
        responseBodyOptionGroup.addOption(responseBodyHtmlOption);

        options.addOptionGroup(actionGroup);
        options.addOption(userOption);
        options.addOption(enableAutoReplyOption);
        options.addOption(responseSubjectOption);
        options.addOptionGroup(responseBodyOptionGroup);
        options.addOption(restrictToContactsOption);
        options.addOption(restrictToDomainOption);
        options.addOption(startTimeOption);
        options.addOption(endTimeOption);
    }

    private final CommandLine commandLine;

    public VacationCli(String[] args) throws ParseException {
        DefaultParser parser = new DefaultParser();
        commandLine = parser.parse(options, args);
    }

    public static void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.setOptionComparator(null);

        formatter.printHelp(String.format("%s %s", Constants.APPLICATION_NAME, HELP_CMD), HELP_HEADER, options, Constants.HELP_FOOTER, true);
    }

    public String getAction() {
        return actionGroup.getSelected();
    }

    public String getUser() throws ParseException {
        String user = commandLine.getOptionValue(Constants.OPTION_USER);

        if (!Constants.EMAIL_PATTERN.matcher(user).matches()) {
            throw new IllegalArgumentException(userOption);
        }

        return user;
    }

    public VacationSettings getVacationSettings() throws ParseException {
        VacationSettings vacationSettings = new VacationSettings();

        if (commandLine.hasOption(Constants.OPTION_ENABLE_AUTO_REPLY)) {
            String enableAutoReply = commandLine.getOptionValue(Constants.OPTION_ENABLE_AUTO_REPLY);

            if (enableAutoReply.equalsIgnoreCase("true")) {
                vacationSettings.setEnableAutoReply(true);
            } else if (enableAutoReply.equalsIgnoreCase("false")) {
                vacationSettings.setEnableAutoReply(false);
            } else {
                throw new IllegalArgumentException(enableAutoReplyOption);
            }
        } else {
            List<String> missingOptions = Collections.singletonList(Constants.OPTION_ENABLE_AUTO_REPLY);

            throw new MissingOptionException(missingOptions);
        }

        if (commandLine.hasOption(Constants.OPTION_START_TIME)) {
            DateString startDateString = new DateString(commandLine.getOptionValue(Constants.OPTION_START_TIME), timeZone);

            try {
                vacationSettings.setStartTime(startDateString.parse());
            } catch (java.text.ParseException e) {
                throw new IllegalArgumentException(startTimeOption);
            }
        }

        if (commandLine.hasOption(Constants.OPTION_END_TIME)) {
            DateString endDateString = new DateString(commandLine.getOptionValue(Constants.OPTION_END_TIME), timeZone);

            try {
                vacationSettings.setEndTime(endDateString.parse());
            } catch (java.text.ParseException e) {
                throw new IllegalArgumentException(endTimeOption);
            }
        }

        if (commandLine.hasOption(Constants.OPTION_RESPONSE_SUBJECT)) {
            String responseSubject = commandLine.getOptionValue(Constants.OPTION_RESPONSE_SUBJECT);

            vacationSettings.setResponseSubject(responseSubject);
        }

        if (commandLine.hasOption(Constants.OPTION_RESPONSE_BODY_PLAIN_TEXT)) {
            String responseBodyPlainText = commandLine.getOptionValue(Constants.OPTION_RESPONSE_BODY_PLAIN_TEXT);

            vacationSettings.setResponseBodyPlainText(responseBodyPlainText);
        } else if (commandLine.hasOption(Constants.OPTION_RESPONSE_BODY_HTML)) {
            String responseBodyHtml = commandLine.getOptionValue(Constants.OPTION_RESPONSE_BODY_HTML);

            vacationSettings.setResponseBodyHtml(responseBodyHtml);
        }

        if (commandLine.hasOption(Constants.OPTION_RESTRICT_TO_CONTACTS)) {
            String restrictToContacts = commandLine.getOptionValue(Constants.OPTION_RESTRICT_TO_CONTACTS);

            if (restrictToContacts.equalsIgnoreCase("true")) {
                vacationSettings.setRestrictToContacts(true);
            } else if (restrictToContacts.equalsIgnoreCase("false")) {
                vacationSettings.setRestrictToContacts(false);
            } else {
                throw new IllegalArgumentException(restrictToContactsOption);
            }
        }

        if (commandLine.hasOption(Constants.OPTION_RESTRICT_TO_DOMAIN)) {
            String restrictToDomain = commandLine.getOptionValue(Constants.OPTION_RESTRICT_TO_DOMAIN);

            if (restrictToDomain.equalsIgnoreCase("true")) {
                vacationSettings.setRestrictToDomain(true);
            } else if (restrictToDomain.equalsIgnoreCase("false")) {
                vacationSettings.setRestrictToDomain(false);
            } else {
                throw new IllegalArgumentException(restrictToDomainOption);
            }
        }

        return vacationSettings;
    }
}
