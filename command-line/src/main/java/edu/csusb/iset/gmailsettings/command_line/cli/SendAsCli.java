package edu.csusb.iset.gmailsettings.command_line.cli;

import com.google.api.services.gmail.model.SendAs;
import edu.csusb.iset.gmailsettings.core.Constants;
import org.apache.commons.cli.*;

import java.util.Collections;
import java.util.List;

public class SendAsCli {

    public static final String HELP_CMD = "sendAs";
    public static final String HELP_HEADER = "\nManages send-as addresses for a user.\n\n";

    private static final Option createOption = Option.builder(Constants.ACTION_CREATE)
            .desc("Create a send-as address")
            .build();
    private static final Option deleteOption = Option.builder(Constants.ACTION_DELETE)
            .desc("Delete the specified send-as address")
            .build();
    private static final Option listOption = Option.builder(Constants.ACTION_LIST)
            .desc("List send-as addresses")
            .build();
    private static final Option updateOption = Option.builder(Constants.ACTION_UPDATE)
            .desc("Update a send-as address")
            .build();
    private static final Option userOption = Option.builder(Constants.OPTION_USER)
            .hasArg()
            .argName("email")
            .desc("Specifies the email address of the user.")
            .required()
            .build();
    private static final Option sendAsEmailOption = Option.builder(Constants.OPTION_SEND_AS_EMAIL)
            .hasArg()
            .argName("email")
            .desc("The email address that appears in the From: header for mail sent using this alias. Address must be in the local domain.")
            .build();
    private static final Option displayNameOption = Option.builder(Constants.OPTION_DISPLAY_NAME)
            .hasArg()
            .argName("name")
            .desc("A name that appears in the From: header for mail sent using this alias. For custom "
                    + "from addresses, when this is empty, Gmail will populate the From: header with the "
                    + "name that is used for the primary address associated with the account. If the admin "
                    + "has disabled the ability for users to update their name format, requests to update this "
                    + "field for the primary login will silently fail. Not used with delete.")
            .build();
    private static final Option replyToAddressOption = Option.builder(Constants.OPTION_REPLY_TO_ADDRESS)
            .hasArg()
            .argName("email")
            .desc("An optional email address that is included in a Reply-To: header for mail sent using "
                    + "this alias. Address must be in the local domain. If this is empty, Gmail will not generate a Reply-To: header. Not used with delete.")
            .build();
    private static final Option signatureOption = Option.builder(Constants.OPTION_SIGNATURE)
            .hasArg()
            .argName("signature")
            .desc("An optional HTML signature that is included in messages composed with this alias in "
                    + "the Gmail web UI. Not used with delete.")
            .build();
    private static final Option isDefaultOption = Option.builder(Constants.OPTION_IS_DEFAULT)
            .hasArg()
            .argName("boolean")
            .desc("Whether this address is selected as the default From: address in situations such as "
                    + "composing a new message or sending a vacation auto-reply. Every Gmail account has "
                    + "exactly one default send-as address. Selecting this option will result in this "
                    + "field becoming false for the other previous default address. Not used with delete.")
            .build();
    private static final Option treatAsAliasOption = Option.builder(Constants.OPTION_TREAT_AS_ALIAS)
            .hasArg()
            .argName("boolean")
            .desc("Whether Gmail should treat this address as an alias for the user's primary email "
                    + "address. This setting only applies to custom from aliases. Not used with delete.")
            .build();

    private static final OptionGroup actionGroup = new OptionGroup();
    private static final Options options = new Options();

    static {
        actionGroup.addOption(createOption);
        actionGroup.addOption(deleteOption);
        actionGroup.addOption(listOption);
        actionGroup.addOption(updateOption);
        actionGroup.setRequired(true);

        options.addOptionGroup(actionGroup);
        options.addOption(userOption);
        options.addOption(sendAsEmailOption);
        options.addOption(displayNameOption);
        options.addOption(replyToAddressOption);
        options.addOption(signatureOption);
        options.addOption(isDefaultOption);
        options.addOption(treatAsAliasOption);
    }

    private final CommandLine commandLine;

    public SendAsCli(String[] args) throws ParseException {
        DefaultParser parser = new DefaultParser();
        commandLine = parser.parse(options, args);
    }

    public static void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.setOptionComparator(null);

        formatter.printHelp(String.format("%s %s", Constants.APPLICATION_NAME, HELP_CMD), HELP_HEADER, options, Constants.HELP_FOOTER, true);

    }

    public String getAction() {
        return actionGroup.getSelected();
    }

    public String getUser() throws ParseException {
        String user = commandLine.getOptionValue(Constants.OPTION_USER);

        if (!Constants.EMAIL_PATTERN.matcher(user).matches()) {
            throw new IllegalArgumentException(userOption);
        }

        return user;
    }

    public String getSendAsEmail() throws ParseException {
        String sendAsEmail;

        if (commandLine.hasOption(Constants.OPTION_SEND_AS_EMAIL)) {
            sendAsEmail = commandLine.getOptionValue(Constants.OPTION_SEND_AS_EMAIL);
        } else {
            List<String> missingOptions = Collections.singletonList(Constants.OPTION_SEND_AS_EMAIL);

            throw new MissingOptionException(missingOptions);
        }

        return sendAsEmail;
    }

    public SendAs getSendAs() throws ParseException {
        SendAs sendAs = new SendAs();

        if (commandLine.hasOption(Constants.OPTION_SEND_AS_EMAIL)) {
            String sendAsEmail = commandLine.getOptionValue(Constants.OPTION_SEND_AS_EMAIL);

            if (Constants.EMAIL_PATTERN.matcher(sendAsEmail).matches()) {
                sendAs.setSendAsEmail(sendAsEmail);
            } else {
                throw new IllegalArgumentException(sendAsEmailOption);
            }

            if (commandLine.hasOption(Constants.ACTION_DELETE)) {
                return sendAs; //Delete action only needs sendAsEmail
            }

            if (commandLine.hasOption(Constants.OPTION_DISPLAY_NAME) && commandLine.hasOption(Constants.OPTION_TREAT_AS_ALIAS)) {
                String displayName = commandLine.getOptionValue(Constants.OPTION_DISPLAY_NAME);
                String treatAsAlias = commandLine.getOptionValue(Constants.OPTION_TREAT_AS_ALIAS);

                sendAs.setDisplayName(displayName);

                if (treatAsAlias.equalsIgnoreCase("true")) {
                    sendAs.setTreatAsAlias(true);
                } else if (treatAsAlias.equalsIgnoreCase("false")) {
                    sendAs.setTreatAsAlias(false);
                } else {
                    throw new IllegalArgumentException(treatAsAliasOption);
                }

                if (commandLine.hasOption(Constants.OPTION_REPLY_TO_ADDRESS)) {
                    String replyToAddress = commandLine.getOptionValue(Constants.OPTION_REPLY_TO_ADDRESS);

                    if (Constants.EMAIL_PATTERN.matcher(replyToAddress).matches()) {
                        sendAs.setReplyToAddress(replyToAddress);
                    } else {
                        throw new IllegalArgumentException(replyToAddressOption);
                    }
                }
            }

            if (commandLine.hasOption(Constants.OPTION_IS_DEFAULT)) {
                String isDefault = commandLine.getOptionValue(Constants.OPTION_IS_DEFAULT);

                if (isDefault.equalsIgnoreCase("true")) {
                    sendAs.setIsDefault(true);
                } else if (isDefault.equalsIgnoreCase("false")) {
                    sendAs.setIsDefault(false);
                } else {
                    throw new IllegalArgumentException(isDefaultOption);
                }
            }
        } else {
            List<String> missingOptions = Collections.singletonList(Constants.OPTION_SEND_AS_EMAIL);

            throw new MissingOptionException(missingOptions);
        }

        return sendAs;
    }
}
